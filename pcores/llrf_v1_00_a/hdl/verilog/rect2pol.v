`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 			FRIB
// Engineer: 			Nathan Usher
// 
// Create Date:    08:49:28 07/05/2011 
// Design Name: 		FRIB LLRF PED2 - FGPDB
// Module Name:    rect2pol 
// Project Name: 		FRIB LLRF PED2
// Description: 		converts I and Q pairs into phase and amplitude
// Additional Comments: 
//		This block expects new inputs valid on ctr == 5'b11110 and produces outputs
//		valid on ctr == 0
//		All regs must be marked as signed, since '>>>' operator is used
//////////////////////////////////////////////////////////////////////////////////
module rect2pol(
	input clk,
	input [4:0] ctr,
	input signed [31:0] lookup_p,
	input signed [31:0] lookup_n,
	input signed [31:0] I,
	input signed [31:0] Q,
	output reg [31:0] mag = 0,
	output reg [31:0] phase = 0
	);

	parameter samples = 29;

	wire signed [31:0] I1, I2;
	wire signed [31:0] Q1, Q2;
	reg signed [31:0] I3 = 0, P = 0;
	reg signed [31:0] Q3 = 0;
	
	assign I1 = Q[31] ? -Q : Q;
	assign Q1 = Q[31] ? I : -I;
	assign I2 = Q3[31] ? (I3 - (Q3 >>> ctr)) : (I3 + (Q3 >>> ctr));	// This line causes a Pack:266 warning
	assign Q2 = Q3[31] ? (Q3 + (I3 >> ctr)) : (Q3 - (I3 >> ctr));		// This line causes a Pack:266 warning
	
	wire done;
	assign done = (ctr == (samples - 1));
	
	always @ (posedge clk)
	begin
		if (done)
		begin
			I3 <= I1;
			Q3 <= Q1;
			P <= Q[31] ? lookup_n : lookup_p;
			phase <= P;
			mag <= I3;
		end
		else
		begin
			I3 <= I2;
			Q3 <= Q2;
			P <= Q3[31] ? (P + lookup_n) : (P + lookup_p);
		end
	end
	
/*	wire [35:0] csCtrl;
	wire [255:0] csData;
	chipscope_icon csIcon(.CONTROL0(csCtrl));
	chipscope_ila csIla(.CONTROL(csCtrl), .CLK(clk), .TRIG0(csData));
	assign csData[31:0] = lookup_p;
	assign csData[63:32] = lookup_n;
	assign csData[79:64] = I[31:16];
	assign csData[95:80] = Q[31:16];
	assign csData[111:96] = mag[31:16];
	assign csData[127:112] = phase[31:16];
	assign csData[143:128] = I1[31:16];
	assign csData[159:144] = Q1[31:16];
	assign csData[175:160] = I2[31:16];
	assign csData[191:176] = Q2[31:16];
	assign csData[207:192] = I3[31:16];
	assign csData[223:208] = Q3[31:16];
	assign csData[239:224] = P[31:16];
	assign csData[244:240] = ctr;
	assign csData[255:245] = 0;*/
endmodule
