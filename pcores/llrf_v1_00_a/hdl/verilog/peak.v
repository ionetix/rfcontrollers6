`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 			Ionetix Corporation
// Engineer: 			Nathan Usher
// 
// Create Date:    09:45:52 02/25/2016 
// Design Name: 	
// Module Name:    peak 
// Project Name: 	
// Description: 
// Additional Comments: 
//		output produced on ctr==5'b11101, valid on ctr==5'b11110
//////////////////////////////////////////////////////////////////////////////////
module peakDetect(
	input clk,
	input [4:0] ctr,
	input signed [13:0] data,
	output reg signed [31:0] peak
	);
	
	parameter samples = 29;

	reg [17:0] absData;
	wire [31:0] dataScaled;
	// Search for the peak value in each set of samples and return it
	DSP48A1 #(.A0REG(1), .A1REG(1), .B0REG(1), .B1REG(1), .CARRYINREG(0), .CARRYINSEL("OPMODE5"),
		.CARRYOUTREG(0), .CREG(0), .DREG(0), .MREG(1), .OPMODEREG(0), .PREG(1), .RSTTYPE("SYNC")
		) peakScale (.BCOUT(), .PCOUT(), .CARRYOUT(), .CARRYOUTF(), .M(), .P(dataScaled), .PCIN(48'h0),
		.CLK(clk), .OPMODE(8'h01), .A(absData),
		.B(18'h05F7F), .C(48'h0), .CARRYIN(1'b0), .D(18'h0), .CEA(1'b1),
		.CEB(1'b1), .CEC(1'b1), .CECARRYIN(1'b1), .CED(1'b1), .CEM(1'b1), .CEOPMODE(1'b1), .CEP(1'b1),
		.RSTA(1'b0), .RSTB(1'b0), .RSTC(1'b0), .RSTCARRYIN(1'b0), .RSTD(1'b0), .RSTM(1'b0),
		.RSTOPMODE(1'b0), .RSTP(1'b0));

	reg [31:0] tempPeak;
	always @ (posedge clk)
	begin
		absData <= data[13] ? {~data, 4'h0} : {data, 4'h0};
		if (ctr == (samples - 2))
		begin
			tempPeak <= dataScaled;
			peak <= peak + (tempPeak >> 6) - (peak >> 6);
		end
		else if (dataScaled > tempPeak)
			tempPeak <= dataScaled;
	end
endmodule
