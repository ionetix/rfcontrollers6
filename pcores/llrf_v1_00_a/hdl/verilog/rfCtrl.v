`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 			FRIB
// Engineer: 			Nathan Usher
// 
// Create Date:    15:55:45 02/20/2013 
// Design Name: 		FRIB LLRF PED2 - FGPDB
// Module Name:    rfCtrl 
// Project Name: 		FRIB LLRF PED2
// Description: 		RF control logic
// Additional Comments: 
//
//////////////////////////////////////////////////////////////////////////////////
module rfCtrl(
	input clk,
	input [4:0] ctr,
	input rfLoopback,
	input rfOn,
	input ctrlEn,
	input findingHome,
	input signed [31:0] findHomeI,
	input signed [31:0] findHomeQ,
	input [31:0] initialTime,
	input [31:0] rampRate,
	input signed [31:0] initialI,
	input signed [31:0] initialQ,
	input signed [31:0] cavI,
	input signed [31:0] cavQ,
	input signed [31:0] setI,
	input signed [31:0] setQ,
	input [31:0] feedFwdGain,
	output reg initialState = 0,
	output reg rampState = 0,
	output reg signed [31:0] ctrlI = 0,
	output reg signed [31:0] ctrlQ = 0
	);

	wire [31:0] max = 32'h36F656C4;
	wire [31:12] adrcI, adrcQ;
	reg signed [31:0] setI2 = 0;
	reg signed [31:0] setQ2 = 0;
	reg [7:0] setI2Lsb = 0, setQ2Lsb = 0;
	reg state = 0;
	reg [31:0] delay = 0;
	reg signed [31:0] ctrlINoGain = 0;
	reg signed [31:0] ctrlQNoGain = 0;
	wire signed [31:0] ctrlIUnsat;
	wire signed [31:0] ctrlQUnsat;
	wire [15:0] overflowI, overflowQ;
	reg [1:0] ctrlEnReg = 0;
	always @ (posedge clk)
	begin
		ctrlEnReg <= {ctrlEnReg[0], ctrlEn};
		initialState <= rfOn & ~state & ~rfLoopback;
		rampState <= state & ((setI != setI2) | (setQ != setQ2)) & ~rfLoopback;

		if (rfLoopback)
		begin
			ctrlI <= max;
			ctrlQ <= 0;
			setI2 <= 0;
			setQ2 <= 0;
			setI2Lsb <= 0;
			setQ2Lsb <= 0;
			state <= 0;
			delay <= 0;
		end
		else if (~rfOn)
		begin
			ctrlI <= 0;
			ctrlQ <= 0;
			setI2 <= 0;
			setQ2 <= 0;
			setI2Lsb <= 0;
			setQ2Lsb <= 0;
			state <= 0;
			delay <= 0;
		end
		else if (findingHome)
		begin
			ctrlINoGain <= findHomeI;
			ctrlQNoGain <= findHomeQ;
			ctrlI <= ~overflowI[15] & ({overflowI, ctrlIUnsat} > max) ? max : (overflowI[15] & (~{overflowI, ctrlIUnsat} > max) ? ~max : ctrlIUnsat);
			ctrlQ <= ~overflowQ[15] & ({overflowQ, ctrlQUnsat} > max) ? max : (overflowQ[15] & (~{overflowQ, ctrlQUnsat} > max) ? ~max : ctrlQUnsat);
			state <= 0;
			delay <= 0;
		end
		else
		begin
			// initial state (force control to open loop in this state)
			if (~state)
			begin
				delay <= delay + 1'b1;
				setI2Lsb <= 0;
				setQ2Lsb <= 0;
				if (delay == initialTime)
					state <= state + 1'b1;
				if (ctrlEnReg[0] & (delay == initialTime))
				begin
					setI2 <= cavI;
					setQ2 <= cavQ;
				end
				else
				begin
					setI2 <= initialI;
					setQ2 <= initialQ;
				end
			end
			// ramp / steady state
			else
			begin
				if (ctrlEnReg[0] & ~ctrlEnReg[1])
				begin
					setI2 <= cavI;
					setQ2 <= cavQ;
				end
				delay <= 0;
				if (setI > setI2)
				begin
					if (({setI2, setI2Lsb} + rampRate) < {setI, 8'h00})
						{setI2, setI2Lsb} <= {setI2, setI2Lsb} + rampRate;
					else
						{setI2, setI2Lsb} <= {setI, 8'h00};
				end
				else if (setI < setI2)
				begin
					if (({setI2, setI2Lsb} - rampRate) > {setI, 8'h00})
						{setI2, setI2Lsb} <= {setI2, setI2Lsb} - rampRate;
					else
						{setI2, setI2Lsb} <= {setI, 8'h00};
				end
				if (setQ > setQ2)
				begin
					if (({setQ2, setQ2Lsb} + rampRate) < {setQ, 8'h00})
						{setQ2, setQ2Lsb} <= {setQ2, setQ2Lsb} + rampRate;
					else
						{setQ2, setQ2Lsb} <= {setQ, 8'h00};
				end
				else if (setQ < setQ2)
				begin
					if (({setQ2, setQ2Lsb} - rampRate) > {setQ, 8'h00})
						{setQ2, setQ2Lsb} <= {setQ2, setQ2Lsb} - rampRate;
					else
						{setQ2, setQ2Lsb} <= {setQ, 8'h00};
				end
			end
		
			ctrlINoGain <= (ctrlEnReg[0] & state) ? {adrcI[31:12], 12'h0} : setI2;
			ctrlQNoGain <= (ctrlEnReg[0] & state) ? {adrcQ[31:12], 12'h0} : setQ2;
			ctrlI <= ~overflowI[15] & ({overflowI, ctrlIUnsat} > max) ? max : (overflowI[15] & (~{overflowI, ctrlIUnsat} > max) ? ~max : ctrlIUnsat);
			ctrlQ <= ~overflowQ[15] & ({overflowQ, ctrlQUnsat} > max) ? max : (overflowQ[15] & (~{overflowQ, ctrlQUnsat} > max) ? ~max : ctrlQUnsat);
		end
	end

	ADRC_IQ adrcCtrlI(.clk(clk), .reset(~rfOn | ~ctrlEnReg[0] | ~state), .ctr(ctr), .set(setI2[31:12]),
		.fdbk(cavI[31:12]), .ctrl(adrcI[31:12]));
	ADRC_IQ adrcCtrlQ(.clk(clk), .reset(~rfOn | ~ctrlEnReg[0] | ~state), .ctr(ctr), .set(setQ2[31:12]),
		.fdbk(cavQ[31:12]), .ctrl(adrcQ[31:12]));

	wire [15:0] shiftOutI, shiftOutQ;
	multS32U32_8cycle multFeedFwdI (.clk(clk), .a(ctrlINoGain), .b(feedFwdGain), .p({overflowI, ctrlIUnsat, shiftOutI}));
	multS32U32_8cycle multFeedFwdQ (.clk(clk), .a(ctrlQNoGain), .b(feedFwdGain), .p({overflowQ, ctrlQUnsat, shiftOutQ}));

	wire [35:0] csCtrl;
	wire [255:0] csData;
	chipscope_icon(.CONTROL0(csCtrl));
	chipscope_ila(.CONTROL(csCtrl), .CLK(clk), .TRIG0(csData));
	assign csData[7:0] = setI2Lsb;
	assign csData[39:8] = setI2;
	assign csData[71:40] = setI;
	assign csData[79:72] = setQ2Lsb;
	assign csData[111:80] = setQ2;
	assign csData[143:112] = setQ;
	assign csData[175:144] = rampRate;
	assign csData[207:176] = ctrlI;
	assign csData[239:208] = ctrlQ;
	assign csData[240] = initialState;
	assign csData[241] = rampState;
	assign csData[242] = ctrlEn;
	assign csData[243] = rfOn;
	assign csData[244] = findingHome;
	assign csData[255:245] = 0;
	
/*	assign csData[15:0] = cavI[31:16];
	assign csData[31:16] = cavQ[31:16];
	assign csData[47:32] = setI[31:16];
	assign csData[63:48] = setQ[31:16];
	assign csData[79:64] = setI2[31:16];
	assign csData[95:80] = setQ2[31:16];
	assign csData[111:96] = ctrlI[31:16];
	assign csData[127:112] = ctrlQ[31:16];
	assign csData[143:128] = ctrlINoGain[31:16];
	assign csData[159:144] = ctrlQNoGain[31:16];
	assign csData[175:160] = ctrlIUnsat[31:16];
	assign csData[191:176] = ctrlQUnsat[31:16];
	assign csData[207:192] = overflowI;
	assign csData[223:208] = overflowQ;
	assign csData[239:224] = max[31:16];
	assign csData[240] = rfLoopback;
	assign csData[241] = rfOn;
	assign csData[242] = ctrlEn;
	assign csData[243] = findingHome;
	assign csData[244] = initialState;
	assign csData[245] = rampState;
	assign csData[255:246] = 0;
	assign csData[31:0] = setI;
	assign csData[63:32] = setQ;
	assign csData[95:64] = cavI;
	assign csData[127:96] = cavQ;
	assign csData[159:128] = ctrlI;
	assign csData[191:160] = ctrlQ;
	assign csData[243:192] = 0;
	assign csData[249:244] = ctr;
	assign csData[250] = rfLoopback;
	assign csData[251] = rfOn;
	assign csData[252] = ctrlEn;
	assign csData[253] = findingHome;
	assign csData[254] = initialState;
	assign csData[255] = rampState;*/
endmodule
