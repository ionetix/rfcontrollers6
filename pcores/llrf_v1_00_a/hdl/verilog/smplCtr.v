`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 			FRIB
// Engineer: 			Nathan Usher
// 
// Create Date:    08:44:07 02/19/2013 
// Design Name: 		FRIB LLRF PED2 - FGPDB
// Module Name:    smplCtr 
// Project Name: 		FRIB LLRF PED2
// Description: 		Counters for the CIC filter, Cordic, etc.
// Additional Comments: 
//		The sync signal should be set high after the pll and adc have been
//		configured and the pll sync signal has been sent.  The sync signal should
//		also be set high after any change to rfStep.  The sync operation
//		may take up to about 8192 clk cycles.
//
//		I have not yet added an algorithm to synchronize the output phase (maintain
//		constant feedforward phase) after a power cycle / reboot.
//
//////////////////////////////////////////////////////////////////////////////////
module smplCtr(
	input clk,
	input stall,
	input [4:0] rfStep,
	output reg [4:0] ctr = 0,
	output reg [4:0] rfCtr = 0
	);

	parameter samples = 29;

	reg [2:0] stallSync = 0;
	always @ (posedge clk)
	begin
		stallSync <= {stallSync[1:0], stall};
		if (~^stallSync[2:1])
		begin
			ctr <= (ctr + 1 > (samples - 1)) ? 5'h0 : ctr + 5'h1;
			rfCtr <= (rfCtr + rfStep > (samples - 1)) ? (rfCtr + rfStep - samples) : rfCtr + rfStep;
		end
	end

/*	wire [35:0] csCtrl;
	wire [255:0] csData;
	chipscope_icon csIcon (.CONTROL0(csCtrl));
	chipscope_ila csIla (.CONTROL(csCtrl), .CLK(clk), .TRIG0(csData));
	assign csData[4:0] = rfStep;
	assign csData[9:5] = ctr;
	assign csData[14:10] = rfCtr;
	assign csData[17:15] = stallSync;
	assign csData[18] = stall;
	assign csData[255:19] = 0;*/
endmodule
