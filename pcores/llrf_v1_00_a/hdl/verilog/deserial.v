`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 		FRIB
// Engineer: 		Nathan Usher
// 
// Create Date:    13:34:12 06/30/2011 
// Design Name: 		FRIB LLRF PED2 - FGPDB
// Module Name:    deserial 
// Project Name: 		FRIB LLRF PED2
// Description: 		Deserializers for RF ADC data
// Additional Comments:
//		Configured for ADC in SDR, 14-bit, bytewise, LSB-first
//		Deserializers must be calibrated by setting ADCs to output sync signal,
//		then setting the bitslipCal input high for at least 20 clkDiv cycles.
//////////////////////////////////////////////////////////////////////////////////
module deserial(
	input clkIo,
	input ioce,
	input clkDiv,
	input [7:0] dataIn,
	input bitslipCal,
	output reg calDone = 0,
	output [13:0] dataOut0,
	output [13:0] dataOut1,
	output [13:0] dataOut2,
	output [13:0] dataOut3
	);

	wire [7:0] shift;
	reg [7:0] bitslipMst = 0, bitslipSlv = 0;
	reg [2:0] bitslipCtr = 0;
	wire [13:0] serdesOut0, serdesOut1, serdesOut2, serdesOut3;
	reg [3:0] serdesReg0, serdesReg1, serdesReg2, serdesReg3;
	reg [10:7] serdesReg4, serdesReg5, serdesReg6, serdesReg7;
	
	reg [4:0] patternOk = 0;
	reg calOk = 0;
	reg bitslipCalPrev = 0;
	reg [7:0] bitslipCalCtr = 0;
	
	always @ (posedge clkDiv)
	begin
		bitslipCalPrev <= bitslipCal;
		if (bitslipCal & ~bitslipCalPrev)
			bitslipCalCtr <= bitslipCalCtr + 1'b1;
		patternOk[0] <= serdesOut0 == 14'b01001100100110;
		patternOk[1] <= serdesOut1 == 14'b01001100100110;
		patternOk[2] <= serdesOut2 == 14'b01001100100110;
		patternOk[3] <= serdesOut3 == 14'b01001100100110;
		patternOk[4] <= &patternOk[3:0];
		if (bitslipCal)
			calDone <= patternOk[4];
		calOk <= ~(|bitslipMst | |bitslipSlv);

		if (bitslipCal & ~|bitslipCtr)
		begin
			bitslipSlv[0] <= (serdesOut0[3:0] != 4'b0110);
			bitslipMst[0] <= (serdesOut0[6:4] != 3'b010);
			bitslipSlv[1] <= (serdesOut0[10:7] != 4'b0110);
			bitslipMst[1] <= (serdesOut0[13:11] != 3'b010);
			bitslipSlv[2] <= (serdesOut1[3:0] != 4'b0110);
			bitslipMst[2] <= (serdesOut1[6:4] != 3'b010);
			bitslipSlv[3] <= (serdesOut1[10:7] != 4'b0110);
			bitslipMst[3] <= (serdesOut1[13:11] != 3'b010);
			bitslipSlv[4] <= (serdesOut2[3:0] != 4'b0110);
			bitslipMst[4] <= (serdesOut2[6:4] != 3'b010);
			bitslipSlv[5] <= (serdesOut2[10:7] != 4'b0110);
			bitslipMst[5] <= (serdesOut2[13:11] != 3'b010);
			bitslipSlv[6] <= (serdesOut3[3:0] != 4'b0110);
			bitslipMst[6] <= (serdesOut3[6:4] != 3'b010);
			bitslipSlv[7] <= (serdesOut3[10:7] != 4'b0110);
			bitslipMst[7] <= (serdesOut3[13:11] != 3'b010);
		end
		else
		begin
			bitslipMst <= 0;
			bitslipSlv <= 0;
		end
		if (bitslipCal)
			bitslipCtr <= bitslipCtr + 1;
		else
			bitslipCtr <= 0;

		serdesReg0[3:0] <= serdesOut0[3:0];
		serdesReg1[3:0] <= serdesOut1[3:0];
		serdesReg2[3:0] <= serdesOut2[3:0];
		serdesReg3[3:0] <= serdesOut3[3:0];
		serdesReg4[10:7] <= serdesOut0[10:7];
		serdesReg5[10:7] <= serdesOut1[10:7];
		serdesReg6[10:7] <= serdesOut2[10:7];
		serdesReg7[10:7] <= serdesOut3[10:7];
	end
	assign dataOut0 = {serdesOut0[13:11], serdesReg4[10:7], serdesOut0[6:4], serdesReg0[3:0]};
	assign dataOut1 = {serdesOut1[13:11], serdesReg5[10:7], serdesOut1[6:4], serdesReg1[3:0]};
	assign dataOut2 = {serdesOut2[13:11], serdesReg6[10:7], serdesOut2[6:4], serdesReg2[3:0]};
	assign dataOut3 = {serdesOut3[13:11], serdesReg7[10:7], serdesOut3[6:4], serdesReg3[3:0]};
	
	// deserialization of "A0" ADC output
   ISERDES2 #(.BITSLIP_ENABLE("TRUE"), .DATA_RATE("SDR"), .DATA_WIDTH(7), .INTERFACE_TYPE("RETIMED"), .SERDES_MODE("MASTER")
		) adcA0_P (.CFB0(), .CFB1(), .DFB(), .FABRICOUT(), .INCDEC(), .Q1(serdesOut0[0]), .Q2(serdesOut0[1]), .Q3(serdesOut0[2]), .Q4(serdesOut0[3]),
      .SHIFTOUT(shift[0]), .VALID(), .BITSLIP(bitslipMst[0]), .CE0(1'b1), .CLK0(clkIo), .CLK1(1'b0), .CLKDIV(clkDiv), .D(dataIn[0]),
      .IOCE(ioce), .RST(1'b0), .SHIFTIN(1'b0));
	ISERDES2 #(.BITSLIP_ENABLE("TRUE"), .DATA_RATE("SDR"), .DATA_WIDTH(7), .INTERFACE_TYPE("RETIMED"), .SERDES_MODE("SLAVE")
		) adcA0_N (.CFB0(), .CFB1(), .DFB(), .FABRICOUT(), .INCDEC(), .Q1(), .Q2(serdesOut0[4]), .Q3(serdesOut0[5]), .Q4(serdesOut0[6]),
      .SHIFTOUT(), .VALID(), .BITSLIP(bitslipSlv[0]), .CE0(1'b1), .CLK0(clkIo), .CLK1(1'b0), .CLKDIV(clkDiv), .D(1'b0),
      .IOCE(ioce), .RST(1'b0), .SHIFTIN(shift[0]));
	// deserialization of "A1" ADC output
   ISERDES2 #(.BITSLIP_ENABLE("TRUE"), .DATA_RATE("SDR"), .DATA_WIDTH(7), .INTERFACE_TYPE("RETIMED"), .SERDES_MODE("MASTER")
		) adcA1_P (.CFB0(), .CFB1(), .DFB(), .FABRICOUT(), .INCDEC(), .Q1(serdesOut0[7]), .Q2(serdesOut0[8]), .Q3(serdesOut0[9]), .Q4(serdesOut0[10]),
      .SHIFTOUT(shift[1]), .VALID(), .BITSLIP(bitslipMst[1]), .CE0(1'b1), .CLK0(clkIo), .CLK1(1'b0), .CLKDIV(clkDiv), .D(dataIn[1]),
      .IOCE(ioce), .RST(1'b0), .SHIFTIN(1'b0));
	ISERDES2 #(.BITSLIP_ENABLE("TRUE"), .DATA_RATE("SDR"), .DATA_WIDTH(7), .INTERFACE_TYPE("RETIMED"), .SERDES_MODE("SLAVE")
		) adcA1_N (.CFB0(), .CFB1(), .DFB(), .FABRICOUT(), .INCDEC(), .Q1(), .Q2(serdesOut0[11]), .Q3(serdesOut0[12]), .Q4(serdesOut0[13]),
      .SHIFTOUT(), .VALID(), .BITSLIP(bitslipSlv[1]), .CE0(1'b1), .CLK0(clkIo), .CLK1(1'b0), .CLKDIV(clkDiv), .D(1'b0),
      .IOCE(ioce), .RST(1'b0), .SHIFTIN(shift[1]));
	// deserialization of "B0" ADC output
   ISERDES2 #(.BITSLIP_ENABLE("TRUE"), .DATA_RATE("SDR"), .DATA_WIDTH(7), .INTERFACE_TYPE("RETIMED"), .SERDES_MODE("MASTER")
		) adcB0_P (.CFB0(), .CFB1(), .DFB(), .FABRICOUT(), .INCDEC(), .Q1(serdesOut1[0]), .Q2(serdesOut1[1]), .Q3(serdesOut1[2]), .Q4(serdesOut1[3]),
      .SHIFTOUT(shift[2]), .VALID(), .BITSLIP(bitslipMst[2]), .CE0(1'b1), .CLK0(clkIo), .CLK1(1'b0), .CLKDIV(clkDiv), .D(dataIn[2]),
      .IOCE(ioce), .RST(1'b0), .SHIFTIN(1'b0));
	ISERDES2 #(.BITSLIP_ENABLE("TRUE"), .DATA_RATE("SDR"), .DATA_WIDTH(7), .INTERFACE_TYPE("RETIMED"), .SERDES_MODE("SLAVE")
		) adcB0_N (.CFB0(), .CFB1(), .DFB(), .FABRICOUT(), .INCDEC(), .Q1(), .Q2(serdesOut1[4]), .Q3(serdesOut1[5]), .Q4(serdesOut1[6]),
      .SHIFTOUT(), .VALID(), .BITSLIP(bitslipSlv[2]), .CE0(1'b1), .CLK0(clkIo), .CLK1(1'b0), .CLKDIV(clkDiv), .D(1'b0),
      .IOCE(ioce), .RST(1'b0), .SHIFTIN(shift[2]));
	// deserialization of "B1" ADC output
   ISERDES2 #(.BITSLIP_ENABLE("TRUE"), .DATA_RATE("SDR"), .DATA_WIDTH(7), .INTERFACE_TYPE("RETIMED"), .SERDES_MODE("MASTER")
		) adcB1_P (.CFB0(), .CFB1(), .DFB(), .FABRICOUT(), .INCDEC(), .Q1(serdesOut1[7]), .Q2(serdesOut1[8]), .Q3(serdesOut1[9]), .Q4(serdesOut1[10]),
      .SHIFTOUT(shift[3]), .VALID(), .BITSLIP(bitslipMst[3]), .CE0(1'b1), .CLK0(clkIo), .CLK1(1'b0), .CLKDIV(clkDiv), .D(dataIn[3]),
      .IOCE(ioce), .RST(1'b0), .SHIFTIN(1'b0));
	ISERDES2 #(.BITSLIP_ENABLE("TRUE"), .DATA_RATE("SDR"), .DATA_WIDTH(7), .INTERFACE_TYPE("RETIMED"), .SERDES_MODE("SLAVE")
		) adcB1_N (.CFB0(), .CFB1(), .DFB(), .FABRICOUT(), .INCDEC(), .Q1(), .Q2(serdesOut1[11]), .Q3(serdesOut1[12]), .Q4(serdesOut1[13]),
      .SHIFTOUT(), .VALID(), .BITSLIP(bitslipSlv[3]), .CE0(1'b1), .CLK0(clkIo), .CLK1(1'b0), .CLKDIV(clkDiv), .D(1'b0),
      .IOCE(ioce), .RST(1'b0), .SHIFTIN(shift[3]));
	// deserialization of "C0" ADC output
   ISERDES2 #(.BITSLIP_ENABLE("TRUE"), .DATA_RATE("SDR"), .DATA_WIDTH(7), .INTERFACE_TYPE("RETIMED"), .SERDES_MODE("MASTER")
		) adcC0_P (.CFB0(), .CFB1(), .DFB(), .FABRICOUT(), .INCDEC(), .Q1(serdesOut2[0]), .Q2(serdesOut2[1]), .Q3(serdesOut2[2]), .Q4(serdesOut2[3]),
      .SHIFTOUT(shift[4]), .VALID(), .BITSLIP(bitslipMst[4]), .CE0(1'b1), .CLK0(clkIo), .CLK1(1'b0), .CLKDIV(clkDiv), .D(dataIn[4]),
      .IOCE(ioce), .RST(1'b0), .SHIFTIN(1'b0));
	ISERDES2 #(.BITSLIP_ENABLE("TRUE"), .DATA_RATE("SDR"), .DATA_WIDTH(7), .INTERFACE_TYPE("RETIMED"), .SERDES_MODE("SLAVE")
		) adcC0_N (.CFB0(), .CFB1(), .DFB(), .FABRICOUT(), .INCDEC(), .Q1(), .Q2(serdesOut2[4]), .Q3(serdesOut2[5]), .Q4(serdesOut2[6]),
      .SHIFTOUT(), .VALID(), .BITSLIP(bitslipSlv[4]), .CE0(1'b1), .CLK0(clkIo), .CLK1(1'b0), .CLKDIV(clkDiv), .D(1'b0),
      .IOCE(ioce), .RST(1'b0), .SHIFTIN(shift[4]));
	// deserialization of "C1" ADC output
   ISERDES2 #(.BITSLIP_ENABLE("TRUE"), .DATA_RATE("SDR"), .DATA_WIDTH(7), .INTERFACE_TYPE("RETIMED"), .SERDES_MODE("MASTER")
		) adcC1_P (.CFB0(), .CFB1(), .DFB(), .FABRICOUT(), .INCDEC(), .Q1(serdesOut2[7]), .Q2(serdesOut2[8]), .Q3(serdesOut2[9]), .Q4(serdesOut2[10]),
      .SHIFTOUT(shift[5]), .VALID(), .BITSLIP(bitslipMst[5]), .CE0(1'b1), .CLK0(clkIo), .CLK1(1'b0), .CLKDIV(clkDiv), .D(dataIn[5]),
      .IOCE(ioce), .RST(1'b0), .SHIFTIN(1'b0));
	ISERDES2 #(.BITSLIP_ENABLE("TRUE"), .DATA_RATE("SDR"), .DATA_WIDTH(7), .INTERFACE_TYPE("RETIMED"), .SERDES_MODE("SLAVE")
		) adcC1_N (.CFB0(), .CFB1(), .DFB(), .FABRICOUT(), .INCDEC(), .Q1(), .Q2(serdesOut2[11]), .Q3(serdesOut2[12]), .Q4(serdesOut2[13]),
      .SHIFTOUT(), .VALID(), .BITSLIP(bitslipSlv[5]), .CE0(1'b1), .CLK0(clkIo), .CLK1(1'b0), .CLKDIV(clkDiv), .D(1'b0),
      .IOCE(ioce), .RST(1'b0), .SHIFTIN(shift[5]));
	// deserialization of "D0" ADC output
   ISERDES2 #(.BITSLIP_ENABLE("TRUE"), .DATA_RATE("SDR"), .DATA_WIDTH(7), .INTERFACE_TYPE("RETIMED"), .SERDES_MODE("MASTER")
		) adcD0_P (.CFB0(), .CFB1(), .DFB(), .FABRICOUT(), .INCDEC(), .Q1(serdesOut3[0]), .Q2(serdesOut3[1]), .Q3(serdesOut3[2]), .Q4(serdesOut3[3]),
      .SHIFTOUT(shift[6]), .VALID(), .BITSLIP(bitslipMst[6]), .CE0(1'b1), .CLK0(clkIo), .CLK1(1'b0), .CLKDIV(clkDiv), .D(dataIn[6]),
      .IOCE(ioce), .RST(1'b0), .SHIFTIN(1'b0));
	ISERDES2 #(.BITSLIP_ENABLE("TRUE"), .DATA_RATE("SDR"), .DATA_WIDTH(7), .INTERFACE_TYPE("RETIMED"), .SERDES_MODE("SLAVE")
		) adcD0_N (.CFB0(), .CFB1(), .DFB(), .FABRICOUT(), .INCDEC(), .Q1(), .Q2(serdesOut3[4]), .Q3(serdesOut3[5]), .Q4(serdesOut3[6]),
      .SHIFTOUT(), .VALID(), .BITSLIP(bitslipSlv[6]), .CE0(1'b1), .CLK0(clkIo), .CLK1(1'b0), .CLKDIV(clkDiv), .D(1'b0),
      .IOCE(ioce), .RST(1'b0), .SHIFTIN(shift[6]));
	// deserialization of "D1" ADC output
   ISERDES2 #(.BITSLIP_ENABLE("TRUE"), .DATA_RATE("SDR"), .DATA_WIDTH(7), .INTERFACE_TYPE("RETIMED"), .SERDES_MODE("MASTER")
		) adcD1_P (.CFB0(), .CFB1(), .DFB(), .FABRICOUT(), .INCDEC(), .Q1(serdesOut3[7]), .Q2(serdesOut3[8]), .Q3(serdesOut3[9]), .Q4(serdesOut3[10]),
      .SHIFTOUT(shift[7]), .VALID(), .BITSLIP(bitslipMst[7]), .CE0(1'b1), .CLK0(clkIo), .CLK1(1'b0), .CLKDIV(clkDiv), .D(dataIn[7]),
      .IOCE(ioce), .RST(1'b0), .SHIFTIN(1'b0));
	ISERDES2 #(.BITSLIP_ENABLE("TRUE"), .DATA_RATE("SDR"), .DATA_WIDTH(7), .INTERFACE_TYPE("RETIMED"), .SERDES_MODE("SLAVE")
		) adcD1_N (.CFB0(), .CFB1(), .DFB(), .FABRICOUT(), .INCDEC(), .Q1(), .Q2(serdesOut3[11]), .Q3(serdesOut3[12]), .Q4(serdesOut3[13]),
      .SHIFTOUT(), .VALID(), .BITSLIP(bitslipSlv[7]), .CE0(1'b1), .CLK0(clkIo), .CLK1(1'b0), .CLKDIV(clkDiv), .D(1'b0),
      .IOCE(ioce), .RST(1'b0), .SHIFTIN(shift[7]));
endmodule
