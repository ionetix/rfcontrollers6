`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 			FRIB
// Engineer: 			Nathan Usher
// 
// Create Date:    15:45:52 06/30/2011 
// Design Name: 		FRIB LLRF PED2 - FGPDB
// Module Name:    cic 
// Project Name: 		FRIB LLRF PED2
// Description: 
// Additional Comments: 
//		output produced on ctr==5'b11101, valid on ctr==5'b11110
//////////////////////////////////////////////////////////////////////////////////
module cic(
	input clk,
	input [4:0] ctr,
	input signed [15:0] cosine,
	input signed [15:0] sine,
	input signed [13:0] data,
	output reg signed [31:0] I,
	output reg signed [31:0] Q
	);
	
	parameter samples = 29;

	// Mix the signal down to baseband
	// These signals should only need to be 29 bits (14bit * 16bit -> 29 bit)
	// Need 34 bits for integrator (29 + 5)
	wire signed [31:0] mult_I_out, mult_Q_out;
	DSP48A1 #(.A0REG(1), .A1REG(1), .B0REG(1), .B1REG(1), .CARRYINREG(0), .CARRYINSEL("OPMODE5"),
		.CARRYOUTREG(0), .CREG(0), .DREG(0), .MREG(1), .OPMODEREG(0), .PREG(1), .RSTTYPE("SYNC")
		) i_dsp48a1 (.BCOUT(), .PCOUT(), .CARRYOUT(), .CARRYOUTF(), .M(), .P(mult_I_out), .PCIN(48'h0),
		.CLK(clk), .OPMODE(8'h01), .A({data[13], data[13], data[13], data[13], data}),
		.B({cosine[15], cosine[15], cosine}), .C(48'h0), .CARRYIN(1'b0), .D(18'h0), .CEA(1'b1),
		.CEB(1'b1), .CEC(1'b1), .CECARRYIN(1'b1), .CED(1'b1), .CEM(1'b1), .CEOPMODE(1'b1), .CEP(1'b1),
		.RSTA(1'b0), .RSTB(1'b0), .RSTC(1'b0), .RSTCARRYIN(1'b0), .RSTD(1'b0), .RSTM(1'b0),
		.RSTOPMODE(1'b0), .RSTP(1'b0));
	DSP48A1 #(.A0REG(1), .A1REG(1), .B0REG(1), .B1REG(1), .CARRYINREG(0), .CARRYINSEL("OPMODE5"),
		.CARRYOUTREG(0), .CREG(0), .DREG(0), .MREG(1), .OPMODEREG(0), .PREG(1), .RSTTYPE("SYNC")
		) q_dsp48a1 (.BCOUT(), .PCOUT(), .CARRYOUT(), .CARRYOUTF(), .M(), .P(mult_Q_out), .PCIN(48'h0),
		.CLK(clk), .OPMODE(8'h01), .A({data[13], data[13], data[13], data[13], data}),
		.B({sine[15], sine[15], sine}), .C(48'h0), .CARRYIN(1'b0), .D(18'h0), .CEA(1'b1),
		.CEB(1'b1), .CEC(1'b1), .CECARRYIN(1'b1), .CED(1'b1), .CEM(1'b1), .CEOPMODE(1'b1), .CEP(1'b1),
		.RSTA(1'b0), .RSTB(1'b0), .RSTC(1'b0), .RSTCARRYIN(1'b0), .RSTD(1'b0), .RSTM(1'b0),
		.RSTOPMODE(1'b0), .RSTP(1'b0));
	
	// CIC filter
	// Integrator needs to be 34 bits (truncate later)
	reg signed [31:0] int_I = 0, int_Q = 0;
	reg signed [31:0] comb_I = 0, comb_Q = 0;
	reg signed [31:0] Iinst = 0, Qinst = 0;
	always @ (posedge clk)
	begin
		int_I <= int_I + {mult_I_out[31], mult_I_out[31:1]};
		int_Q <= int_Q + {mult_Q_out[31], mult_Q_out[31:1]};

		if (ctr==(samples - 2))
		begin
			comb_I <= int_I + {mult_I_out[31], mult_I_out[31:1]};
			comb_Q <= int_Q + {mult_Q_out[31], mult_Q_out[31:1]};
//			I <= int_I + {mult_I_out[31], mult_I_out[31:1]} - comb_I;
//			Q <= int_Q + {mult_Q_out[31], mult_Q_out[31:1]} - comb_Q;
			Iinst <= int_I + {mult_I_out[31], mult_I_out[31:1]} - comb_I;
			Qinst <= int_Q + {mult_Q_out[31], mult_Q_out[31:1]} - comb_Q;
			I <= I + (Iinst >>> 6) - (I >>> 6);
			Q <= Q + (Qinst >>> 6) - (Q >>> 6);
		end
	end
	
/*	wire csClk;
	wire [35:0] csCtrl;
	wire [255:0] csData;
	chipscope_icon csIcon (.CONTROL0(csCtrl));
	chipscope_ila csIla (.CONTROL(csCtrl), .CLK(csClk), .TRIG0(csData));
	assign csClk = clk;
	assign csData[31:0] = I;
	assign csData[63:32] = Q;
	assign csData[95:64] = mult_I_out;
	assign csData[127:96] = mult_Q_out;
	assign csData[143:128] = sine;
	assign csData[159:144] = cosine;
	assign csData[173:160] = data;
	assign csData[178:174] = ctr;
	assign csData[255:179] = 0;*/
endmodule
