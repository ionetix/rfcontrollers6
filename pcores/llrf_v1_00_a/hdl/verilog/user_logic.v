//----------------------------------------------------------------------------
// user_logic.v - module
//----------------------------------------------------------------------------
//
// ***************************************************************************
// ** Copyright (c) 1995-2012 Xilinx, Inc.  All rights reserved.            **
// **                                                                       **
// ** Xilinx, Inc.                                                          **
// ** XILINX IS PROVIDING THIS DESIGN, CODE, OR INFORMATION "AS IS"         **
// ** AS A COURTESY TO YOU, SOLELY FOR USE IN DEVELOPING PROGRAMS AND       **
// ** SOLUTIONS FOR XILINX DEVICES.  BY PROVIDING THIS DESIGN, CODE,        **
// ** OR INFORMATION AS ONE POSSIBLE IMPLEMENTATION OF THIS FEATURE,        **
// ** APPLICATION OR STANDARD, XILINX IS MAKING NO REPRESENTATION           **
// ** THAT THIS IMPLEMENTATION IS FREE FROM ANY CLAIMS OF INFRINGEMENT,     **
// ** AND YOU ARE RESPONSIBLE FOR OBTAINING ANY RIGHTS YOU MAY REQUIRE      **
// ** FOR YOUR IMPLEMENTATION.  XILINX EXPRESSLY DISCLAIMS ANY              **
// ** WARRANTY WHATSOEVER WITH RESPECT TO THE ADEQUACY OF THE               **
// ** IMPLEMENTATION, INCLUDING BUT NOT LIMITED TO ANY WARRANTIES OR        **
// ** REPRESENTATIONS THAT THIS IMPLEMENTATION IS FREE FROM CLAIMS OF       **
// ** INFRINGEMENT, IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS       **
// ** FOR A PARTICULAR PURPOSE.                                             **
// **                                                                       **
// ***************************************************************************
//
//----------------------------------------------------------------------------
// Filename:          user_logic.v
// Version:           1.00.a
// Description:       User logic module.
// Date:              Thu Mar 27 22:21:24 2014 (by Create and Import Peripheral Wizard)
// Verilog Standard:  Verilog-2001
//----------------------------------------------------------------------------
// Naming Conventions:
//   active low signals:                    "*_n"
//   clock signals:                         "clk", "clk_div#", "clk_#x"
//   reset signals:                         "rst", "rst_n"
//   generics:                              "C_*"
//   user defined types:                    "*_TYPE"
//   state machine next state:              "*_ns"
//   state machine current state:           "*_cs"
//   combinatorial signals:                 "*_com"
//   pipelined or register delay signals:   "*_d#"
//   counter signals:                       "*cnt*"
//   clock enable signals:                  "*_ce"
//   internal version of output port:       "*_i"
//   device pins:                           "*_pin"
//   ports:                                 "- Names begin with Uppercase"
//   processes:                             "*_PROCESS"
//   component instantiations:              "<ENTITY_>I_<#|FUNC>"
//----------------------------------------------------------------------------

module user_logic
(
  // -- ADD USER PORTS BELOW THIS LINE ---------------
  CLK_DAC_N,
  CLK_DAC_P,
  DAC_N,
  DAC_P,
  DAC_SLEEP,
  CLK12_5,
  RF_ON_MON,
  PLC_RFON,
  PLC_RFLOCKED,
  SPIDAC_CLK,
  SPIDAC_CS,
  SPIDAC_DATA,
  LEDS_CLK,
  LEDS_DATA,
  LEDS_LATCH,
  ADC_CLKBIT_N,
  ADC_CLKBIT_P,
  ADC_A0_N,
  ADC_A0_P,
  ADC_A1_N,
  ADC_A1_P,
  ADC_B0_N,
  ADC_B0_P,
  ADC_B1_N,
  ADC_B1_P,
  ADC_C0_N,
  ADC_C0_P,
  ADC_C1_N,
  ADC_C1_P,
  ADC_D0_N,
  ADC_D0_P,
  ADC_D1_N,
  ADC_D1_P,
  SW_LOOPBACK_N,
  SW_LOOPBACK_P,
  OUTPUT_EN,
  bitSlipCal,
  configDone,
  pll0locked,
  pll1locked,
  serdesCalDone,
  tunerDn,
  tunerUp,
  tunerGoHome,
  tunerSetHome,
  tunerMoving,
  tunerFault,
  atten,
  // -- ADD USER PORTS ABOVE THIS LINE ---------------

  // -- DO NOT EDIT BELOW THIS LINE ------------------
  // -- Bus protocol ports, do not add to or delete 
  Bus2IP_Clk,                     // Bus to IP clock
  Bus2IP_Resetn,                  // Bus to IP reset
  Bus2IP_Addr,                    // Bus to IP address bus
  Bus2IP_CS,                      // Bus to IP chip select for user logic memory selection
  Bus2IP_RNW,                     // Bus to IP read/not write
  Bus2IP_Data,                    // Bus to IP data bus
  Bus2IP_BE,                      // Bus to IP byte enables
  Bus2IP_RdCE,                    // Bus to IP read chip enable
  Bus2IP_WrCE,                    // Bus to IP write chip enable
  Bus2IP_Burst,                   // Bus to IP burst-mode qualifier
  Bus2IP_BurstLength,             // Bus to IP burst length
  Bus2IP_RdReq,                   // Bus to IP read request
  Bus2IP_WrReq,                   // Bus to IP write request
  Type_of_xfer,                   // Transfer Type
  IP2Bus_AddrAck,                 // IP to Bus address acknowledgement
  IP2Bus_Data,                    // IP to Bus data bus
  IP2Bus_RdAck,                   // IP to Bus read transfer acknowledgement
  IP2Bus_WrAck,                   // IP to Bus write transfer acknowledgement
  IP2Bus_Error                    // IP to Bus error response
  // -- DO NOT EDIT ABOVE THIS LINE ------------------
); // user_logic

// -- ADD USER PARAMETERS BELOW THIS LINE ------------
parameter saturate                       = 32'h4D9F6FEE;
parameter samples                        = 29;
// -- ADD USER PARAMETERS ABOVE THIS LINE ------------

// -- DO NOT EDIT BELOW THIS LINE --------------------
// -- Bus protocol parameters, do not add to or delete
parameter C_SLV_AWIDTH                   = 32;
parameter C_SLV_DWIDTH                   = 32;
parameter C_NUM_MEM                      = 1;
// -- DO NOT EDIT ABOVE THIS LINE --------------------

// -- ADD USER PORTS BELOW THIS LINE -----------------
input                                     CLK_DAC_N;
input                                     CLK_DAC_P;
output [13:0]                             DAC_N;
output [13:0]                             DAC_P;
output                                    DAC_SLEEP;
input                                     CLK12_5;
output												RF_ON_MON;
output                                    PLC_RFON;
output                                    PLC_RFLOCKED;
output                                    SPIDAC_CLK;
output                                    SPIDAC_CS;
output                                    SPIDAC_DATA;
output                                    LEDS_CLK;
output                                    LEDS_DATA;
output                                    LEDS_LATCH;
input                                     ADC_CLKBIT_N;
input                                     ADC_CLKBIT_P;
input                                     ADC_A0_N;
input                                     ADC_A0_P;
input                                     ADC_A1_N;
input                                     ADC_A1_P;
input                                     ADC_B0_N;
input                                     ADC_B0_P;
input                                     ADC_B1_N;
input                                     ADC_B1_P;
input                                     ADC_C0_N;
input                                     ADC_C0_P;
input                                     ADC_C1_N;
input                                     ADC_C1_P;
input                                     ADC_D0_N;
input                                     ADC_D0_P;
input                                     ADC_D1_N;
input                                     ADC_D1_P;
output                                    SW_LOOPBACK_N;
output                                    SW_LOOPBACK_P;
input                                     OUTPUT_EN;
input                                     bitSlipCal;
input                                     configDone;
input                                     pll0locked;
input                                     pll1locked;
output                                    serdesCalDone;
output reg                                tunerDn = 0;
output reg                                tunerUp = 0;
output reg                                tunerGoHome = 0;
output reg                                tunerSetHome = 0;
input                                     tunerMoving;
input                                     tunerFault;
input  [5:0]                              atten;
// -- ADD USER PORTS ABOVE THIS LINE -----------------

// -- DO NOT EDIT BELOW THIS LINE --------------------
// -- Bus protocol ports, do not add to or delete
input                                     Bus2IP_Clk;
input                                     Bus2IP_Resetn;
input      [C_SLV_AWIDTH-1 : 0]           Bus2IP_Addr;
input      [C_NUM_MEM-1 : 0]              Bus2IP_CS;
input                                     Bus2IP_RNW;
input      [C_SLV_DWIDTH-1 : 0]           Bus2IP_Data;
input      [C_SLV_DWIDTH/8-1 : 0]         Bus2IP_BE;
input      [C_NUM_MEM-1 : 0]              Bus2IP_RdCE;
input      [C_NUM_MEM-1 : 0]              Bus2IP_WrCE;
input                                     Bus2IP_Burst;
input      [7 : 0]                        Bus2IP_BurstLength;
input                                     Bus2IP_RdReq;
input                                     Bus2IP_WrReq;
input                                     Type_of_xfer;
output                                    IP2Bus_AddrAck;
output     [C_SLV_DWIDTH-1 : 0]           IP2Bus_Data;
output                                    IP2Bus_RdAck;
output                                    IP2Bus_WrAck;
output                                    IP2Bus_Error;
// -- DO NOT EDIT ABOVE THIS LINE --------------------

//----------------------------------------------------------------------------
// Implementation
//----------------------------------------------------------------------------

	// registers accessible by the CPU
	wire signed [31:0] refPhase;																	// reference to adc sample clock phase
	reg signed [31:0] fwdPhaseRaw = 0, rflPhaseRaw, cavPhaseRaw = 0;					// raw input phases to reference phase
	reg signed [31:0] fwdPhaseDev = 0, cavPhaseDev = 0;									// input phases to reference phase (at SMA connector)
	reg signed [31:0] fwdPhaseSys = 0, cavPhaseSys = 0;									// input phases to reference phase (at pickup)
	reg signed [31:0] fwdDevOfst = 32'hD0E000000, cavDevOfst = 32'hD0E00000;										// device phase calibration offsets
	reg signed [31:0] fwdSysOfst = 0, cavSysOfst = 0;										// system phase calibration offsets
	wire [31:0] fwdAmplRaw, rflAmplRaw, cavAmplRaw, refAmplRaw;							// input amplitudes (uncalibrated)
	reg [31:0] fwdAmplDev = 0, rflAmplDev = 0, cavAmplDev = 0, refAmplDev;			// input amplitudes calibrated to Vp at SMA connector (8.24 bit fixed point)
	reg [31:0] fwdAmplSys = 0, rflAmplSys = 0, cavAmplSys = 0, refAmplSys;			// input amplitude calibrated to Vp at pickup (16.16 bit fixed point)
	reg [31:0] maxFwdDev = 0, maxRflDev = 0, maxCavDev = 0, maxRefDev = 0;			// maximum input levels before ADC saturates
	reg [31:0] maxFwdSys = 0, maxRflSys = 0, maxCavSys = 0, maxRefSys = 0;			// maximum input levels before ADC saturates
	reg [31:0] fwdDevCal = 32'h0000091F, rflDevCal = 32'h00000479;						// LLRF calibrations (raw value after CORDIC to Vp at SMA connector)
	reg [31:0] cavDevCal = 32'h0000125F, refDevCal = 32'h0000026E;						// LLRF calibrations (raw value after CORDIC to Vp at SMA connector)
	reg [31:0] fwdSysCal = 32'h00010000, rflSysCal = 32'h00010000;						// System calibration (LLRF val to reflected power at cavity input)
	reg [31:0] cavSysCal = 32'h00010000, refSysCal = 32'h00010000;						// System calibration (LLRF val to volts in cavity)
	wire signed [31:0] ctrlPhase;																	// output phase to DAC
	reg [31:0] setPhase = 0;																		// phase setpoint
	reg [31:0] rfSetPhase1 = 0, rfSetPhase2 = 0, rfSetPhase = 0;						// register for phase setpoint in rf clock domain
	wire [31:0] ctrlAmpl;																				// output amplitude to DAC
	reg [31:0] initialAmpl = 32'h13880000, setAmpl = 32'h16CA0000;						// amplitude setpoints (default to ~10dB to make initial calibration faster)
	reg [31:0] rfSetAmpl1 = 0, rfSetAmpl2 = 0, rfSetAmpl = 0;							// registers for amplitude setpoint in rf clock domain
	reg [31:0] initialTime = 32'h0C085E38;														// time to remain at initial amplitude
	reg [31:0] findHomeAmpl = 32'h05DC0000;													// amplitude when finding home
	reg [31:0] rampRate = 32'h00000100;															// ramp rate on amplitude changes
	reg signed [31:0] feedFwdPhase = 0;															// feedfwd phase (not used yet)
	reg signed [31:0] startupPhase = 0;															// feedfwd phase within LLRF (changes when LLRF is rebooted)
	reg [31:0] feedFwdGain = 32'h00010000;														// feedfwd gain
	wire rfOn, rfInitial, rfRamp, ilkd;															// RF status bits
	reg rfSetOn = 0, ctrlEn = 0;																	// RF mode settings
	reg findingHome = 0;																				// status of findHome operation
	reg [7:0] ilkResetBus = 0, ilkCtrResetBus = 0;											// shift registers to send pulses to ilk module to reset interlocks
	reg [7:0] ilkTurnsOffRf = 0;																	// mask of interlocks that turn off RF
	reg [7:0] ilkRateLimit = 8'hFF;																// interlock rate interlock threshold
	reg [6:0] ilkRateMask = 0;																		// mask of interlocks that are used for interlock rate interlock
	reg [5:0] ilkRateShift = 6'h3F;																// timescale to use for interlock rate interlock
	reg [15:0] ilkResetSel = 16'h0000;	                          // mask that sets how each interlock is reset
	reg [31:0] fwdRflMinRfl = 32'h0046B5EF;													// minimum forward power for V+/V- interlock to be enabled
	reg [31:0] cavLowMinFwd = 32'h0046C000;													// minimum forward power for cav low interlock to be enabled
	reg [23:0] fwdRflRatio1Unsat = 24'h000200;
	reg [23:0] fwdRflRatio2Unsat = 24'h000200;
	reg [23:0] initFwdRflRatioUnsat = 24'h000200;
	wire [15:0] fwdRflRatio1;
	wire [15:0] fwdRflRatio2;
	wire [15:0] initFwdRflRatio;
	assign fwdRflRatio1 = |fwdRflRatio1Unsat[23:16] ? 16'hFFFF : fwdRflRatio1Unsat[15:0];
	assign fwdRflRatio2 = |fwdRflRatio2Unsat[23:16] ? 16'hFFFF : fwdRflRatio2Unsat[15:0];
	assign initFwdRflRatio = |initFwdRflRatioUnsat[23:16] ? 16'hFFFF : initFwdRflRatioUnsat[15:0];
	reg [31:0] fwdRflTime1 = 32'h00B8D274;														// V+/V- interlock threshold (interlock 1)
	reg [31:0] fwdRflTime2 = 32'h07383888;														// V+/V- interlock threshold (interlock 2)
	reg [31:0] initFwdRflTime = 32'h026812D8;													// V+/V- interlock threshold
	reg [31:0] dvdtRate = 32'h00000400;															// -dV/dt interlock threshold
	reg ilkReset = 0, ilkCtrReset = 0;															// reset signal for interlocks
	wire setRfOff;																						// signal that turns off RF setting
	wire [7:0] firstIlk, ilkLatches, ilkSts;													// interlock status masks
	wire [127:0] ilkCtrs;																			// interlock counters
	reg [31:0] fastResetTime = 32'h04D025B0, slowResetTime = 32'hFFFFFFFE;			// interlock reset times
	reg [31:0] cavLowAmpl = 32'h09C40000, cavLowTime = 32'h00B8D274;					// cav low interlock thresholds
	reg [31:0] fwdCavPhase = 32'h00000000;														// phase difference between fwd and cav when cavity is tuned correctly
	reg [31:0] tunerDeadZone = 32'h00B60B61;													// minimum error for tuner to move
	reg [31:0] tunerHomeDelay = 32'h00400000;													// delay after RF off before tuner goes to home position
	reg tunerEn = 0;																					// tuner enable
	reg rfLoopback = 0;																				// loopback mode used to measure feedfwd phase components coming from inside the LLRF
	reg loadDevCal = 0;																				// flag written and read by CPU - the firmware doesn't do anything with it
	reg loadSysCal = 0;																				// flag written and read by CPU - the firmware doesn't do anything with it
	reg loadSettings = 0;																			// flag written and read by CPU - the firmware doesn't do anything with it
	reg saveDevCal = 0;																				// flag written and read by CPU - the firmware doesn't do anything with it
	reg saveSysCal = 0;																				// flag written and read by CPU - the firmware doesn't do anything with it
	reg saveSettings = 0;																			// flag written and read by CPU - the firmware doesn't do anything with it
	reg [4:0] rfStep = 1;																			// step size for CIC lookup table
	reg stall = 0;																						// when this changes state, the ADC sample counters stall for one clock cycle
	
	// All registers visible by the CPU that are in other clock domains are frozen during reads to ensure valid data is read
	// The CPU sets "freeze" before every read and resets it after every read
	// Device calibration registers are locked unless the embedded software sets the unlockCal flag
	wire rfClk;
	wire [4:0] ctr;
	reg freeze = 0;
	reg unlockCal = 0;
	reg [1:0] freezeReg = 0;
	reg signed [31:0] refPhase2Bus = 0;
	reg signed [31:0] fwdPhaseRaw2Bus = 0, rflPhaseRaw2Bus = 0, cavPhaseRaw2Bus = 0;
	reg signed [31:0] fwdPhaseDev2Bus = 0, cavPhaseDev2Bus = 0;
	reg signed [31:0] fwdPhaseSys2Bus = 0, cavPhaseSys2Bus = 0;
	reg [31:0] fwdAmplRaw2Bus = 0, rflAmplRaw2Bus = 0, cavAmplRaw2Bus = 0, refAmplRaw2Bus = 0;
	reg [31:0] fwdAmplDev2Bus = 0, rflAmplDev2Bus = 0, cavAmplDev2Bus = 0, refAmplDev2Bus = 0;
	reg [31:0] fwdAmplSys2Bus = 0, rflAmplSys2Bus = 0, cavAmplSys2Bus = 0, refAmplSys2Bus = 0;
	reg [31:0] ctrlPhase2Bus = 0, ctrlAmpl2Bus = 0;
	reg [127:0] ilkCtrs2Bus = 0;
	reg [7:0] firstIlk2Bus = 0, ilkLatches2Bus = 0, ilkSts2Bus = 0;
	reg [31:0] adcClkRate = 32'hFFFFFFFF;
	always @ (posedge rfClk)
	begin
		freezeReg <= {freezeReg[0], freeze};
		if (~freezeReg[1] & ~|ctr)
		begin
			refPhase2Bus		<= refPhase;
			fwdPhaseRaw2Bus	<= fwdPhaseRaw;
			rflPhaseRaw2Bus	<= rflPhaseRaw;
			cavPhaseRaw2Bus	<= cavPhaseRaw;
			fwdPhaseDev2Bus	<= fwdPhaseDev;
			cavPhaseDev2Bus	<= cavPhaseDev;
			fwdPhaseSys2Bus	<= fwdPhaseSys;
			cavPhaseSys2Bus	<= cavPhaseSys;
			fwdAmplRaw2Bus		<= fwdAmplRaw;
			rflAmplRaw2Bus		<= rflAmplRaw;
			cavAmplRaw2Bus		<= cavAmplRaw;
			refAmplRaw2Bus		<= refAmplRaw;
			fwdAmplDev2Bus		<= fwdAmplDev;
			rflAmplDev2Bus		<= rflAmplDev;
			cavAmplDev2Bus		<= cavAmplDev;
			refAmplDev2Bus		<= refAmplDev;
			fwdAmplSys2Bus		<= fwdAmplSys;
			rflAmplSys2Bus		<= rflAmplSys;
			cavAmplSys2Bus		<= cavAmplSys;
			refAmplSys2Bus		<= refAmplSys;
			ctrlPhase2Bus		<= ctrlPhase;
			ctrlAmpl2Bus		<= ctrlAmpl;
			ilkCtrs2Bus			<= ilkCtrs;
			firstIlk2Bus		<= firstIlk;
			ilkLatches2Bus		<= ilkLatches;
			ilkSts2Bus			<= ilkSts;
		end
	end

	// AXI read command
	reg [31:0] dataToBus;
	always @*
	begin
		case (Bus2IP_Addr[9:2])
			8'h00:	dataToBus <= fwdAmplSys2Bus;						// forward amplitude (system)
			8'h01:	dataToBus <= rflAmplSys2Bus;						// reflected amplitude (system)
			8'h02:	dataToBus <= cavAmplSys2Bus;						// cavity amplitude (system)
			8'h03:	dataToBus <= refAmplSys2Bus;						// reference amplitude (system)
			8'h04:	dataToBus <= fwdAmplDev2Bus;						// forward amplitude (device)
			8'h05:	dataToBus <= rflAmplDev2Bus;						// reflected amplitude (device)
			8'h06:	dataToBus <= cavAmplDev2Bus;						// cavity amplitude (device)
			8'h07:	dataToBus <= refAmplDev2Bus;						// reference amplitude (device)
			8'h08:	dataToBus <= fwdAmplRaw2Bus;						// forward amplitude (raw)
			8'h09:	dataToBus <= rflAmplRaw2Bus;						// reflected amplitude (raw)
			8'h0A:	dataToBus <= cavAmplRaw2Bus;						// cavity amplitude (raw)
			8'h0B:	dataToBus <= refAmplRaw2Bus;						// reference amplitude (raw)
			8'h0C:	dataToBus <= ctrlAmpl2Bus;							// output amplitude (from control block)
			8'h10:	dataToBus <= fwdPhaseSys2Bus;						// forward-reference phase (system)
			8'h11:	dataToBus <= 0;										// reflected-reference phase (system)
			8'h12:	dataToBus <= cavPhaseSys2Bus;						// cavity phase-reference (system)
			8'h13:	dataToBus <= refPhase2Bus;							// reference phase (relative to adc sample clock)
			8'h14:	dataToBus <= fwdPhaseDev2Bus;						// forward-reference phase (device)
			8'h15:	dataToBus <= 0;										// reflected-reference phase (device)
			8'h16:	dataToBus <= cavPhaseDev2Bus;						// cavity-reference phase (device)
			8'h17:	dataToBus <= refPhase2Bus;							// reference phase (relative to adc sample clock)
			8'h18:	dataToBus <= fwdPhaseRaw2Bus;						// forward-reference phase (raw)
			8'h19:	dataToBus <= rflPhaseRaw2Bus;						// reflected-reference phase (raw)
			8'h1A:	dataToBus <= cavPhaseRaw2Bus;						// cavity-reference phase (raw)
			8'h1B:	dataToBus <= refPhase2Bus;							// reference phase (relative to adc sample clock)
			8'h1C:	dataToBus <= ctrlPhase2Bus;						// output phase (from control block)
			8'h20:	dataToBus <= fwdSysCal;								// fwd amplitude calibration (value at SMA connector to value at end of cable)
			8'h21:	dataToBus <= rflSysCal;								// rfl amplitude calibration (value at SMA connector to value at end of cable)
			8'h22:	dataToBus <= cavSysCal;								// cav amplitude calibration (value at SMA connector to value at end of cable)
			8'h23:	dataToBus <= refSysCal;								// cav amplitude calibration (value at SMA connector to value at end of cable)
			8'h24:	dataToBus <= fwdDevCal;								// fwd amplitude calibration (raw value after CORDIC to Vp at SMA connector)
			8'h25:	dataToBus <= rflDevCal;								// rfl amplitude calibration (raw value after CORDIC to Vp at SMA connector)
			8'h26:	dataToBus <= cavDevCal;								// cav amplitude calibration (raw value after CORDIC to Vp at SMA connector)
			8'h27:	dataToBus <= refDevCal;								// cav amplitude calibration (raw value after CORDIC to Vp at SMA connector)
			8'h28:	dataToBus <= fwdSysOfst;							// fwd phase calibration (value at SMA connector to value at end of cable)
			8'h29:	dataToBus <= 0;										// rfl phase calibration (value at SMA connector to value at end of cable)
			8'h2A:	dataToBus <= cavSysOfst;							// cav phase calibration (value at SMA connector to value at end of cable)
			8'h2C:	dataToBus <= fwdDevOfst;							// fwd phase calibration (raw value after CORDIC to Vp at SMA connector)
			8'h2D:	dataToBus <= 0;										// rfl phase calibration (raw value after CORDIC to Vp at SMA connector)
			8'h2E:	dataToBus <= cavDevOfst;							// cav phase calibration (raw value after CORDIC to Vp at SMA connector)
			8'h30:	dataToBus <= feedFwdGain;							// feedforward gain setting
			8'h31:	dataToBus <= feedFwdPhase;							// feedforward phase setting
			8'h32:	dataToBus <= initialAmpl;							// initial amplitude setting
			8'h33:	dataToBus <= initialTime;							// time at initial amplitude setting
			8'h34:	dataToBus <= rampRate;								// ramp rate to new setpoints
			8'h35:	dataToBus <= setAmpl;								// amplitude setting (saturated if setting is too high)
			8'h36:	dataToBus <= setPhase;								// phase setting
			8'h37:	dataToBus <= fwdCavPhase;							// offset between forward and cavity phases when cavity is tuned
			8'h38:	dataToBus <= tunerDeadZone;						// minimum phase error before tuner moves
			8'h39:	dataToBus <= tunerHomeDelay;						// delay after RF off before tuner moves to home position
			8'h3A:	dataToBus <= findHomeAmpl;							// output amplitude when finding cavity resonance
			8'h3B:	dataToBus <= startupPhase;							// feedforward phase within LLRF (changes when LLRF is rebooted)
			8'h40:	dataToBus <= fastResetTime;						// shorter time that can be used to automatically reset interlocks
			8'h41:	dataToBus <= slowResetTime;						// longer time that can be used to automatically reset interlocks
			8'h42:	dataToBus <= cavLowAmpl;							// cavity low threshold
			8'h43:	dataToBus <= cavLowTime;							// cavity low time below threshold to trigger interlock
			8'h44:	dataToBus <= fwdRflMinRfl;							// minimum forward amplitude for V+/V- to trip
			8'h45:	dataToBus <= {8'h0, initFwdRflRatio, 8'h0};	// forward to reflected ratio when at initial amplitude
			8'h46:	dataToBus <= {8'h0, fwdRflRatio1, 8'h0};		// forward to reflected ratio
			8'h47:	dataToBus <= {8'h0, fwdRflRatio2, 8'h0};		// forward to reflected ratio
			8'h48:	dataToBus <= initFwdRflTime;						// forward to reflected ratio time when at initial amplitude
			8'h49:	dataToBus <= fwdRflTime1;							// forward to reflected ratio time to trigger interlock
			8'h4A:	dataToBus <= fwdRflTime2;							// forward to reflected ratio time to trigger interlock
			8'h4B:	dataToBus <= dvdtRate;								// -dV/dt rate to trigger interlock
			8'h4C:	dataToBus <= {25'h0, ilkRateMask};				// interlock mask for interlock rate interlock
			8'h4D:	dataToBus <= {26'h0, ilkRateShift};				// interlock rate timescale for moving average
			8'h4E:	dataToBus <= {24'h0, ilkRateLimit};				// interlock rate threshold to interlock
			8'h4F:	dataToBus <= cavLowMinFwd;							// minimum forward amplitude for cav low to trip
			8'h50:	dataToBus <= {ilkCtrs2Bus[15:0], 16'h0};		// cavity low counter
			8'h51:	dataToBus <= {ilkCtrs2Bus[31:16], 16'h0};		// fwd/rfl ratio interlock 1 counter
			8'h52:	dataToBus <= {ilkCtrs2Bus[47:32], 16'h0};		// fwd/rfl ratio interlock 2 counter
			8'h53:	dataToBus <= {ilkCtrs2Bus[63:48], 16'h0};		// -dV/dt interlock counter
			8'h54:	dataToBus <= {ilkCtrs2Bus[79:64], 16'h0};		// PLC interlock counter
			8'h55:	dataToBus <= {ilkCtrs2Bus[95:80], 16'h0};		// PLL unlock counter
			8'h56:	dataToBus <= {ilkCtrs2Bus[111:96], 16'h0};	// tuner fault counter
			8'h57:	dataToBus <= {ilkCtrs2Bus[127:112], 16'h0};	// interlock rate counter
			8'h58:	dataToBus <= {29'h0, ilkTurnsOffRf[0], ilkResetSel[1:0]};	// interlock 0 configuration
			8'h59:	dataToBus <= {29'h0, ilkTurnsOffRf[1], ilkResetSel[3:2]};	// interlock 1 configuration
			8'h5A:	dataToBus <= {29'h0, ilkTurnsOffRf[2], ilkResetSel[5:4]};	// interlock 2 configuration
			8'h5B:	dataToBus <= {29'h0, ilkTurnsOffRf[3], ilkResetSel[7:6]};	// interlock 3 configuration
			8'h5C:	dataToBus <= {29'h0, ilkTurnsOffRf[4], ilkResetSel[9:8]};	// interlock 4 configuration
			8'h5D:	dataToBus <= {29'h0, ilkTurnsOffRf[5], ilkResetSel[11:10]};	// interlock 5 configuration
			8'h5E:	dataToBus <= {29'h0, ilkTurnsOffRf[6], ilkResetSel[13:12]};	// interlock 6 configuration
			8'h5F:	dataToBus <= {29'h0, ilkTurnsOffRf[7], ilkResetSel[15:14]};	// interlock 7 configuration
			8'h60:	dataToBus <= {24'h0, firstIlk2Bus};				// interlock that caused RF to shut off
			8'h61:	dataToBus <= {24'h0, ilkLatches2Bus};			// latched interlocks
			8'h62:	dataToBus <= {24'h0, ilkSts2Bus};				// current interlock status
			8'h63:	dataToBus <= 32'h0;									// interlock reset register (write-only)
			8'h64:	dataToBus <= 32'h0;									// interlock counter reset register (write-only)
			8'h6D:	dataToBus <= {31'h0, loadDevCal};				// flag written and read by CPU - the firmware doesn't do anything with it
			8'h6E:	dataToBus <= {31'h0, loadSysCal};				// flag written and read by CPU - the firmware doesn't do anything with it
			8'h6F:	dataToBus <= {31'h0, loadSettings};				// flag written and read by CPU - the firmware doesn't do anything with it
			8'h70:	dataToBus <= {31'h0, rfSetOn};					// RF on/off setting
			8'h71:	dataToBus <= {31'h0, tunerEn};					// tuner on/off setting
			8'h72:	dataToBus <= {31'h0, ctrlEn};						// RF IQ control on/off setting
			8'h73:	dataToBus <= 0;										// RF feedback fwd/cav setting
			8'h74:	dataToBus <= {31'h0, rfOn};						// RF on/off status
			8'h75:	dataToBus <= {31'h0, rfRamp};						// RF ramp status
			8'h76:	dataToBus <= {31'h0, rfInitial};					// RF initial status
			8'h77:	dataToBus <= {31'h0, ilkd};						// RF interlocked
			8'h78:	dataToBus <= {31'h0, findingHome};				// LLRF is sweeping cavity with tuner to find resonance
			8'h79:	dataToBus <= {31'h0, rfLoopback};				// RF output is being sent back to reflected input channel internally
			8'h7A:	dataToBus <= {31'h0, freeze};						// write 1 to freeze values coming from other clock domains to ensure valid reads
			8'h7B:	dataToBus <= {31'h0, saveDevCal};				// flag written and read by CPU - the firmware doesn't do anything with it
			8'h7C:	dataToBus <= {31'h0, saveSysCal};				// flag written and read by CPU - the firmware doesn't do anything with it
			8'h7D:	dataToBus <= {31'h0, saveSettings};				// flag written and read by CPU - the firmware doesn't do anything with it
			8'h7E:	dataToBus <= {31'h0, unlockCal};					// device calibrations can only be written when this flag is set to 1
			8'h80:	dataToBus <= maxFwdSys;								// maximum forward channel input before ADC saturation
			8'h81:	dataToBus <= maxRflSys;								// maximum reflected channel input before ADC saturation
			8'h82:	dataToBus <= maxCavSys;								// maximum cavity channel input before ADC saturation
			8'h83:	dataToBus <= maxRefSys;								// maximum reference channel input before ADC saturation
			8'h84:	dataToBus <= maxFwdDev;								// maximum forward channel input before ADC saturation
			8'h85:	dataToBus <= maxRflDev;								// maximum reflected channel input before ADC saturation
			8'h86:	dataToBus <= maxCavDev;								// maximum cavity channel input before ADC saturation
			8'h87:	dataToBus <= maxRefDev;								// maximum reference channel input before ADC saturation
			8'h90:	dataToBus <= {27'h0, rfStep};						// step size through CIC lookup table
			8'h91:	dataToBus <= 32'h0;			        	  // selects between 4 and 8 samples per cycle for output DAC (deprecated)
			8'h92:	dataToBus <= 32'h0;									// write to this register to stall the ADC sample counters for one clock cycle
			8'h93:	dataToBus <= adcClkRate;						// rfSmplClk frequency in Hz
			8'hA0:	dataToBus <= 0;										// write-only:	enable RF
			8'hA1:	dataToBus <= 0;										// write-only:	disable RF
			8'hA2:	dataToBus <= 0;										// write-only:	enable tuner
			8'hA3:	dataToBus <= 0;										// write-only:	disable tuner
			8'hA4:	dataToBus <= 0;										// write-only:	enable amplitude control
			8'hA5:	dataToBus <= 0;										// write-only:	disable amplitude control
			8'hA6:	dataToBus <= 0;										// write-only:	set feedback to fwd amplitude
			8'hA7:	dataToBus <= 0;										// write-only:	set feedback to cav amplitude
			8'hA8:	dataToBus <= 0;										// write-only:	find cavity resonance and set tuner home
			8'hA9:	dataToBus <= 0;										// write-only:	abort find home operation
			8'hFE:	dataToBus <= 32'h0;									// returns 0
			8'hFF:	dataToBus <= 32'hFFFFFFFF;							// returns -1
			default:	dataToBus <= 32'hFFFFFFFF;
		endcase
	end
	
	// AXI write command
	always @ (posedge Bus2IP_Clk)
	begin
		// Don't allow ramprate to be set to 0, increment it if it is set to 0
		if (~IP2Bus_WrAck & ~|rampRate)
			rampRate <= 32'h00000001;
		if (~IP2Bus_WrAck & ~|tunerHomeDelay)
			tunerHomeDelay <= 32'h00000001;
		if (~IP2Bus_WrAck & setRfOff)
			rfSetOn <= 0;
		if (~IP2Bus_WrAck)
		begin
			ilkResetBus[0] <= 0;
			ilkCtrResetBus[0] <= 0;
		end
		ilkResetBus[7:1] <= ilkResetBus[6:0];
		ilkReset <= |ilkResetBus;
		ilkCtrResetBus[7:1] <= ilkCtrResetBus[6:0];
		ilkCtrReset <= |ilkCtrResetBus;
		if (IP2Bus_WrAck & Bus2IP_BE[0])
		begin
			case (Bus2IP_Addr[9:2])
				8'h20:	fwdSysCal[7:0]									<= Bus2IP_Data[7:0];
				8'h21:	rflSysCal[7:0]									<= Bus2IP_Data[7:0];
				8'h22:	cavSysCal[7:0]									<= Bus2IP_Data[7:0];
				8'h23:	refSysCal[7:0]									<= Bus2IP_Data[7:0];
				8'h24:	if (unlockCal)	fwdDevCal[7:0]				<= Bus2IP_Data[7:0];
				8'h25:	if (unlockCal)	rflDevCal[7:0]				<= Bus2IP_Data[7:0];
				8'h26:	if (unlockCal)	cavDevCal[7:0]				<= Bus2IP_Data[7:0];
				8'h27:	if (unlockCal)	refDevCal[7:0]				<= Bus2IP_Data[7:0];
				8'h28:	fwdSysOfst[7:0]								<= Bus2IP_Data[7:0];
				8'h2A:	cavSysOfst[7:0]								<= Bus2IP_Data[7:0];
				8'h2C:	if (unlockCal)	fwdDevOfst[7:0]			<= Bus2IP_Data[7:0];
				8'h2E:	if (unlockCal)	cavDevOfst[7:0]			<= Bus2IP_Data[7:0];
				8'h30:	feedFwdGain[7:0]								<= Bus2IP_Data[7:0];
				8'h31:	feedFwdPhase[7:0]								<= Bus2IP_Data[7:0];
				8'h32:	initialAmpl[7:0]								<= Bus2IP_Data[7:0];
				8'h33:	initialTime[7:0]								<= Bus2IP_Data[7:0];
				8'h34:	rampRate[7:0]									<= Bus2IP_Data[7:0];
				8'h35:	setAmpl[7:0]									<= Bus2IP_Data[7:0];
				8'h36:	setPhase[7:0]									<= Bus2IP_Data[7:0];
				8'h37:	fwdCavPhase[7:0]								<= Bus2IP_Data[7:0];
				8'h38:	tunerDeadZone[7:0]							<= Bus2IP_Data[7:0];
				8'h39:	tunerHomeDelay[7:0]							<= Bus2IP_Data[7:0];
				8'h3A:	findHomeAmpl[7:0]								<= Bus2IP_Data[7:0];
				8'h3B:	startupPhase[7:0]								<= Bus2IP_Data[7:0];
				8'h40:	fastResetTime[7:0]							<= Bus2IP_Data[7:0];
				8'h41:	slowResetTime[7:0]							<= Bus2IP_Data[7:0];
				8'h42:	cavLowAmpl[7:0]								<= Bus2IP_Data[7:0];
				8'h43:	cavLowTime[7:0]								<= Bus2IP_Data[7:0];
				8'h44:	fwdRflMinRfl[7:0]								<= Bus2IP_Data[7:0];
				8'h48:	initFwdRflTime[7:0]							<= Bus2IP_Data[7:0];
				8'h49:	fwdRflTime1[7:0]								<= Bus2IP_Data[7:0];
				8'h4A:	fwdRflTime2[7:0]								<= Bus2IP_Data[7:0];
				8'h4B:	dvdtRate[7:0]									<= Bus2IP_Data[7:0];
				8'h4C:	ilkRateMask										<= Bus2IP_Data[6:0];
				8'h4D:	ilkRateShift									<= Bus2IP_Data[5:0];
				8'h4E:	ilkRateLimit									<= Bus2IP_Data[7:0];
				8'h4F:	cavLowMinFwd[7:0]								<= Bus2IP_Data[7:0];
				8'h58:	{ilkTurnsOffRf[0], ilkResetSel[1:0]}	<= Bus2IP_Data[2:0];
				8'h59:	{ilkTurnsOffRf[1], ilkResetSel[3:2]}	<= Bus2IP_Data[2:0];
				8'h5A:	{ilkTurnsOffRf[2], ilkResetSel[5:4]}	<= Bus2IP_Data[2:0];
				8'h5B:	{ilkTurnsOffRf[3], ilkResetSel[7:6]}	<= Bus2IP_Data[2:0];
				8'h5C:	{ilkTurnsOffRf[4], ilkResetSel[9:8]}	<= Bus2IP_Data[2:0];
				8'h5D:	{ilkTurnsOffRf[5], ilkResetSel[11:10]}	<= Bus2IP_Data[2:0];
				8'h5E:	{ilkTurnsOffRf[6], ilkResetSel[13:12]}	<= Bus2IP_Data[2:0];
				8'h5F:	{ilkTurnsOffRf[7], ilkResetSel[15:14]}	<= Bus2IP_Data[2:0];
				8'h63:	ilkResetBus[0]									<= Bus2IP_Data[0];
				8'h64:	ilkCtrResetBus[0]								<= Bus2IP_Data[0];
				8'h6D:	loadDevCal										<= Bus2IP_Data[0];
				8'h6E:	loadSysCal										<= Bus2IP_Data[0];
				8'h6F:	loadSettings									<= Bus2IP_Data[0];
				8'h70:	rfSetOn											<= Bus2IP_Data[0];
				8'h71:	tunerEn											<= Bus2IP_Data[0];
				8'h72:	ctrlEn											<= Bus2IP_Data[0];
				8'h79:	rfLoopback										<= Bus2IP_Data[0];
				8'h7A:	freeze											<= Bus2IP_Data[0];
				8'h7B:	saveDevCal										<= Bus2IP_Data[0];
				8'h7C:	saveSysCal										<= Bus2IP_Data[0];
				8'h7D:	saveSettings									<= Bus2IP_Data[0];
				8'h7E:	unlockCal										<= Bus2IP_Data[0];
				8'h90:	rfStep											<= Bus2IP_Data[4:0];
				8'h92:	stall												<= ~stall;
				8'h93:	adcClkRate[7:0]								<= Bus2IP_Data[7:0];
				8'hA0:	if (Bus2IP_Data[0])		rfSetOn			<= 1;
				8'hA1:	if (Bus2IP_Data[0])		rfSetOn			<= 0;
				8'hA2:	if (Bus2IP_Data[0])		tunerEn			<= 1;
				8'hA3:	if (Bus2IP_Data[0])		tunerEn			<= 0;
				8'hA4:	if (Bus2IP_Data[0])		ctrlEn			<= 1;
				8'hA5:	if (Bus2IP_Data[0])		ctrlEn			<= 0;
			endcase
		end
		if (IP2Bus_WrAck & Bus2IP_BE[1])
		begin
			case (Bus2IP_Addr[9:2])
				8'h20:	fwdSysCal[15:8]								<= Bus2IP_Data[15:8];
				8'h21:	rflSysCal[15:8]								<= Bus2IP_Data[15:8];
				8'h22:	cavSysCal[15:8]								<= Bus2IP_Data[15:8];
				8'h23:	refSysCal[15:8]								<= Bus2IP_Data[15:8];
				8'h24:	if (unlockCal)	fwdDevCal[15:8]			<= Bus2IP_Data[15:8];
				8'h25:	if (unlockCal)	rflDevCal[15:8]			<= Bus2IP_Data[15:8];
				8'h26:	if (unlockCal)	cavDevCal[15:8]			<= Bus2IP_Data[15:8];
				8'h27:	if (unlockCal)	refDevCal[15:8]			<= Bus2IP_Data[15:8];
				8'h28:	fwdSysOfst[15:8]								<= Bus2IP_Data[15:8];
				8'h2A:	cavSysOfst[15:8]								<= Bus2IP_Data[15:8];
				8'h2C:	if (unlockCal)	fwdDevOfst[15:8]			<= Bus2IP_Data[15:8];
				8'h2E:	if (unlockCal)	cavDevOfst[15:8]			<= Bus2IP_Data[15:8];
				8'h30:	feedFwdGain[15:8]								<= Bus2IP_Data[15:8];
				8'h31:	feedFwdPhase[15:8]							<= Bus2IP_Data[15:8];
				8'h32:	initialAmpl[15:8]								<= Bus2IP_Data[15:8];
				8'h33:	initialTime[15:8]								<= Bus2IP_Data[15:8];
				8'h34:	rampRate[15:8]									<= Bus2IP_Data[15:8];
				8'h35:	setAmpl[15:8]									<= Bus2IP_Data[15:8];
				8'h36:	setPhase[15:8]									<= Bus2IP_Data[15:8];
				8'h37:	fwdCavPhase[15:8]								<= Bus2IP_Data[15:8];
				8'h38:	tunerDeadZone[15:8]							<= Bus2IP_Data[15:8];
				8'h39:	tunerHomeDelay[15:8]							<= Bus2IP_Data[15:8];
				8'h3A:	findHomeAmpl[15:8]							<= Bus2IP_Data[15:8];
				8'h3B:	startupPhase[15:8]							<= Bus2IP_Data[15:8];
				8'h40:	fastResetTime[15:8]							<= Bus2IP_Data[15:8];
				8'h41:	slowResetTime[15:8]							<= Bus2IP_Data[15:8];
				8'h42:	cavLowAmpl[15:8]								<= Bus2IP_Data[15:8];
				8'h43:	cavLowTime[15:8]								<= Bus2IP_Data[15:8];
				8'h44:	fwdRflMinRfl[15:8]							<= Bus2IP_Data[15:8];
				8'h45:	initFwdRflRatioUnsat[7:0]					<= Bus2IP_Data[15:8];
				8'h46:	fwdRflRatio1Unsat[7:0]						<= Bus2IP_Data[15:8];
				8'h47:	fwdRflRatio2Unsat[7:0]						<= Bus2IP_Data[15:8];
				8'h48:	initFwdRflTime[15:8]							<= Bus2IP_Data[15:8];
				8'h49:	fwdRflTime1[15:8]								<= Bus2IP_Data[15:8];
				8'h4A:	fwdRflTime2[15:8]								<= Bus2IP_Data[15:8];
				8'h4B:	dvdtRate[15:8]									<= Bus2IP_Data[15:8];
				8'h4F:	cavLowMinFwd[15:8]							<= Bus2IP_Data[15:8];
				8'h93:	adcClkRate[15:8]								<= Bus2IP_Data[15:8];
			endcase
		end
		if (IP2Bus_WrAck & Bus2IP_BE[2])
		begin
			case (Bus2IP_Addr[9:2])
				8'h20:	fwdSysCal[23:16]								<= Bus2IP_Data[23:16];
				8'h21:	rflSysCal[23:16]								<= Bus2IP_Data[23:16];
				8'h22:	cavSysCal[23:16]								<= Bus2IP_Data[23:16];
				8'h23:	refSysCal[23:16]								<= Bus2IP_Data[23:16];
				8'h24:	if (unlockCal)	fwdDevCal[23:16]			<= Bus2IP_Data[23:16];
				8'h25:	if (unlockCal)	rflDevCal[23:16]			<= Bus2IP_Data[23:16];
				8'h26:	if (unlockCal)	cavDevCal[23:16]			<= Bus2IP_Data[23:16];
				8'h27:	if (unlockCal)	refDevCal[23:16]			<= Bus2IP_Data[23:16];
				8'h28:	fwdSysOfst[23:16]								<= Bus2IP_Data[23:16];
				8'h2A:	cavSysOfst[23:16]								<= Bus2IP_Data[23:16];
				8'h2C:	if (unlockCal)	fwdDevOfst[23:16]			<= Bus2IP_Data[23:16];
				8'h2E:	if (unlockCal)	cavDevOfst[23:16]			<= Bus2IP_Data[23:16];
				8'h30:	feedFwdGain[23:16]							<= Bus2IP_Data[23:16];
				8'h31:	feedFwdPhase[23:16]							<= Bus2IP_Data[23:16];
				8'h32:	initialAmpl[23:16]							<= Bus2IP_Data[23:16];
				8'h33:	initialTime[23:16]							<= Bus2IP_Data[23:16];
				8'h34:	rampRate[23:16]								<= Bus2IP_Data[23:16];
				8'h35:	setAmpl[23:16]									<= Bus2IP_Data[23:16];
				8'h36:	setPhase[23:16]								<= Bus2IP_Data[23:16];
				8'h37:	fwdCavPhase[23:16]							<= Bus2IP_Data[23:16];
				8'h38:	tunerDeadZone[23:16]							<= Bus2IP_Data[23:16];
				8'h39:	tunerHomeDelay[23:16]						<= Bus2IP_Data[23:16];
				8'h3A:	findHomeAmpl[23:16]							<= Bus2IP_Data[23:16];
				8'h3B:	startupPhase[23:16]							<= Bus2IP_Data[23:16];
				8'h40:	fastResetTime[23:16]							<= Bus2IP_Data[23:16];
				8'h41:	slowResetTime[23:16]							<= Bus2IP_Data[23:16];
				8'h42:	cavLowAmpl[23:16]								<= Bus2IP_Data[23:16];
				8'h43:	cavLowTime[23:16]								<= Bus2IP_Data[23:16];
				8'h44:	fwdRflMinRfl[23:16]							<= Bus2IP_Data[23:16];
				8'h45:	initFwdRflRatioUnsat[15:8]					<= Bus2IP_Data[23:16];
				8'h46:	fwdRflRatio1Unsat[15:8]						<= Bus2IP_Data[23:16];
				8'h47:	fwdRflRatio2Unsat[15:8]						<= Bus2IP_Data[23:16];
				8'h48:	initFwdRflTime[23:16]						<= Bus2IP_Data[23:16];
				8'h49:	fwdRflTime1[23:16]							<= Bus2IP_Data[23:16];
				8'h4A:	fwdRflTime2[23:16]							<= Bus2IP_Data[23:16];
				8'h4B:	dvdtRate[23:16]								<= Bus2IP_Data[23:16];
				8'h4F:	cavLowMinFwd[23:16]							<= Bus2IP_Data[23:16];
				8'h93:	adcClkRate[23:16]								<= Bus2IP_Data[23:16];
			endcase
		end
		if (IP2Bus_WrAck & Bus2IP_BE[3])
		begin
			case (Bus2IP_Addr[9:2])
				8'h20:	fwdSysCal[31:24]								<= Bus2IP_Data[31:24];
				8'h21:	rflSysCal[31:24]								<= Bus2IP_Data[31:24];
				8'h22:	cavSysCal[31:24]								<= Bus2IP_Data[31:24];
				8'h23:	refSysCal[31:24]								<= Bus2IP_Data[31:24];
				8'h24:	if (unlockCal)	fwdDevCal[31:24]			<= Bus2IP_Data[31:24];
				8'h25:	if (unlockCal)	rflDevCal[31:24]			<= Bus2IP_Data[31:24];
				8'h26:	if (unlockCal)	cavDevCal[31:24]			<= Bus2IP_Data[31:24];
				8'h27:	if (unlockCal)	refDevCal[31:24]			<= Bus2IP_Data[31:24];
				8'h28:	fwdSysOfst[31:24]								<= Bus2IP_Data[31:24];
				8'h2A:	cavSysOfst[31:24]								<= Bus2IP_Data[31:24];
				8'h2C:	if (unlockCal)	fwdDevOfst[31:24]			<= Bus2IP_Data[31:24];
				8'h2E:	if (unlockCal)	cavDevOfst[31:24]			<= Bus2IP_Data[31:24];
				8'h30:	feedFwdGain[31:24]							<= Bus2IP_Data[31:24];
				8'h31:	feedFwdPhase[31:24]							<= Bus2IP_Data[31:24];
				8'h32:	initialAmpl[31:24]							<= Bus2IP_Data[31:24];
				8'h33:	initialTime[31:24]							<= Bus2IP_Data[31:24];
				8'h34:	rampRate[31:24]								<= Bus2IP_Data[31:24];
				8'h35:	setAmpl[31:24]									<= Bus2IP_Data[31:24];
				8'h36:	setPhase[31:24]								<= Bus2IP_Data[31:24];
				8'h37:	fwdCavPhase[31:24]							<= Bus2IP_Data[31:24];
				8'h38:	tunerDeadZone[31:24]							<= Bus2IP_Data[31:24];
				8'h39:	tunerHomeDelay[31:24]						<= Bus2IP_Data[31:24];
				8'h3A:	findHomeAmpl[31:24]							<= Bus2IP_Data[31:24];
				8'h3B:	startupPhase[31:24]							<= Bus2IP_Data[31:24];
				8'h40:	fastResetTime[31:24]							<= Bus2IP_Data[31:24];
				8'h41:	slowResetTime[31:24]							<= Bus2IP_Data[31:24];
				8'h42:	cavLowAmpl[31:24]								<= Bus2IP_Data[31:24];
				8'h43:	cavLowTime[31:24]								<= Bus2IP_Data[31:24];
				8'h44:	fwdRflMinRfl[31:24]							<= Bus2IP_Data[31:24];
				8'h45:	initFwdRflRatioUnsat[23:16]				<= Bus2IP_Data[31:24];
				8'h46:	fwdRflRatio1Unsat[23:16]					<= Bus2IP_Data[31:24];
				8'h47:	fwdRflRatio2Unsat[23:16]					<= Bus2IP_Data[31:24];
				8'h48:	initFwdRflTime[31:24]						<= Bus2IP_Data[31:24];
				8'h49:	fwdRflTime1[31:24]							<= Bus2IP_Data[31:24];
				8'h4A:	fwdRflTime2[31:24]							<= Bus2IP_Data[31:24];
				8'h4B:	dvdtRate[31:24]								<= Bus2IP_Data[31:24];
				8'h4F:	cavLowMinFwd[31:24]							<= Bus2IP_Data[31:24];
				8'h93:	adcClkRate[31:24]								<= Bus2IP_Data[31:24];
			endcase
		end
	end
	
	// Output signals to AXI bus
	assign IP2Bus_Data = IP2Bus_RdAck ? dataToBus : 32'h0;
	assign IP2Bus_AddrAck = Bus2IP_CS[0];
	assign IP2Bus_WrAck = Bus2IP_CS[0] & ~Bus2IP_RNW;
	assign IP2Bus_RdAck = Bus2IP_CS[0] & Bus2IP_RNW;
	assign IP2Bus_Error = 0;

	// ADC bit clock is put on CLKIO path to the serdes.  The divided bit clock is used for RF control logic (rfClk is declared above)
	wire adcClkBit, adcClkIo, adcClkDiv;
	wire serdesStrobe;
	IBUFGDS #(.DIFF_TERM("TRUE"), .IOSTANDARD("LVDS_33")) adcBitClk_ibufgds (.O(adcClkBit), .I(ADC_CLKBIT_P), .IB(ADC_CLKBIT_N));
	BUFIO2 #(.DIVIDE(7), .DIVIDE_BYPASS("FALSE"), .I_INVERT("FALSE"), .USE_DOUBLER("FALSE")
		) adcClkBit_bufio2 (.DIVCLK(adcClkDiv), .IOCLK(adcClkIo), .SERDESSTROBE(serdesStrobe), .I(adcClkBit));
	BUFG adcClkDiv_bufg (.O(rfClk), .I(adcClkDiv));

	// input buffers and delay block for ADC data
	wire [7:0] adcDataToDly, adcData;
	IBUFDS #(.DIFF_TERM("TRUE"), .IOSTANDARD("LVDS_33")) adcDataA0_ibufds (.O(adcDataToDly[0]), .I(ADC_A0_P), .IB(ADC_A0_N));
	IBUFDS #(.DIFF_TERM("TRUE"), .IOSTANDARD("LVDS_33")) adcDataA1_ibufds (.O(adcDataToDly[1]), .I(ADC_A1_P), .IB(ADC_A1_N));
	IBUFDS #(.DIFF_TERM("TRUE"), .IOSTANDARD("LVDS_33")) adcDataB0_ibufds (.O(adcDataToDly[2]), .I(ADC_B0_P), .IB(ADC_B0_N));
	IBUFDS #(.DIFF_TERM("TRUE"), .IOSTANDARD("LVDS_33")) adcDataB1_ibufds (.O(adcDataToDly[3]), .I(ADC_B1_P), .IB(ADC_B1_N));
	IBUFDS #(.DIFF_TERM("TRUE"), .IOSTANDARD("LVDS_33")) adcDataC0_ibufds (.O(adcDataToDly[4]), .I(ADC_C0_P), .IB(ADC_C0_N));
	IBUFDS #(.DIFF_TERM("TRUE"), .IOSTANDARD("LVDS_33")) adcDataC1_ibufds (.O(adcDataToDly[5]), .I(ADC_C1_P), .IB(ADC_C1_N));
	IBUFDS #(.DIFF_TERM("TRUE"), .IOSTANDARD("LVDS_33")) adcDataD0_ibufds (.O(adcDataToDly[6]), .I(ADC_D0_P), .IB(ADC_D0_N));
	IBUFDS #(.DIFF_TERM("TRUE"), .IOSTANDARD("LVDS_33")) adcDataD1_ibufds (.O(adcDataToDly[7]), .I(ADC_D1_P), .IB(ADC_D1_N));
	genvar x;
	generate
		for (x=0; x<8; x=x+1) 
		begin: ibufds
			IODELAY2 #(.DATA_RATE("SDR"), .DELAY_SRC("IDATAIN"), .IDELAY_MODE("NORMAL"), .IDELAY_TYPE("FIXED"), .IDELAY_VALUE(15),
				.SERDES_MODE("MASTER"), .SIM_TAPDELAY_VALUE(75)) adcData_iodelay2 (.BUSY(), .DATAOUT(adcData[x]), .DATAOUT2(),
				.DOUT(), .TOUT(), .CAL(1'b0), .CE(1'b0), .CLK(1'b0), .IDATAIN(adcDataToDly[x]), .INC(1'b0), .IOCLK0(1'b0), .IOCLK1(1'b0),
				.ODATAIN(1'b0), .RST(1'b0), .T(1'b1));
		end
	endgenerate
	
	// the hardware iserdes2 deserializers are used to turn the ADC data stream into parallel data registers
	// parallel data is synchronized to the adc0 frame clock (rfClk)
	wire signed [13:0] fwdRaw, rflRaw, cavRaw, refRaw;	// raw ADC samples
	deserial adc(.clkIo(adcClkIo), .ioce(serdesStrobe), .clkDiv(rfClk), .dataIn(adcData), .bitslipCal(bitSlipCal),
		.calDone(serdesCalDone), .dataOut0(rflRaw), .dataOut1(fwdRaw), .dataOut2(cavRaw), .dataOut3(refRaw));

	// sample counters for CIC filter, CORDIC, etc. (ctr is declared above)
	wire [4:0] rfCtr;
	wire signed [31:0] fwdPhaseAdc, rflPhaseAdc, cavPhaseAdc;	// raw input phases
	smplCtr #(.samples(samples)) counters(.clk(rfClk), .stall(stall), .rfStep(rfStep), .ctr(ctr), .rfCtr(rfCtr));

	// lookup tables for CIC filters and CORDIC blocks
	wire signed [15:0] cosine, sine;
	wire signed [31:0] cordic_P, cordic_N;
	generate
		case (samples)
			19: begin: samples_19
					cic_lookup_table_19 cicLookup(.clka(rfClk), .addra(rfCtr), .douta({cosine, sine}), .clkb(1'b0), .addrb(5'h0), .doutb());
				end
			29: begin: samples_29
					cic_lookup_table_29 cicLookup(.clka(rfClk), .addra(rfCtr), .douta({cosine, sine}), .clkb(1'b0), .addrb(5'h0), .doutb());
				end
			31: begin: samples_31
					cic_lookup_table_31 cicLookup(.clka(rfClk), .addra(rfCtr), .douta({cosine, sine}), .clkb(1'b0), .addrb(5'h0), .doutb());
				end
			default: begin: samples_error
				end
		endcase
	endgenerate
	cordic_lookup_table cordicLookup(.clka(rfClk), .addra({1'b0, ctr}), .douta(cordic_P), .clkb(rfClk), .addrb({1'b1, ctr}), .doutb(cordic_N));

	// CIC filter converts raw ADC samples to I and Q
	// also includes a moving average
	// new CIC outputs are valid on (ctr == samples - 1)
	wire signed [31:0] fwdI, fwdQ, rflI, rflQ, cavI, cavQ, refI, refQ;
	// cosine is negated to make phase increment in correct direction
	cic #(.samples(samples)) makeIqFwd(.clk(rfClk), .ctr(ctr), .cosine(-cosine), .sine(sine), .data(fwdRaw), .I(fwdI), .Q(fwdQ));
	cic #(.samples(samples)) makeIqRfl(.clk(rfClk), .ctr(ctr), .cosine(-cosine), .sine(sine), .data(rflRaw), .I(rflI), .Q(rflQ));
	peakDetect #(.samples(samples)) rflPeakDetect(.clk(rfClk), .ctr(ctr), .data(rflRaw), .peak(rflAmplRaw));
	cic #(.samples(samples)) makeIqCav(.clk(rfClk), .ctr(ctr), .cosine(-cosine), .sine(sine), .data(cavRaw), .I(cavI), .Q(cavQ));
	cic #(.samples(samples)) makeIqRef(.clk(rfClk), .ctr(ctr), .cosine(-cosine), .sine(sine), .data(refRaw), .I(refI), .Q(refQ));

	// convert I and Q samples to phase and magnitude
	// new phase/amplitude values are valid on (ctr == 0)
	rect2pol #(.samples(samples)) cordicFwd(.clk(rfClk), .ctr(ctr), .lookup_p(cordic_P), .lookup_n(cordic_N), .I(fwdI), .Q(fwdQ), .mag(fwdAmplRaw), .phase(fwdPhaseAdc));
	rect2pol #(.samples(samples)) cordicRfl(.clk(rfClk), .ctr(ctr), .lookup_p(cordic_P), .lookup_n(cordic_N), .I(rflI), .Q(rflQ), .mag(), .phase(rflPhaseAdc));
	rect2pol #(.samples(samples)) cordicCav(.clk(rfClk), .ctr(ctr), .lookup_p(cordic_P), .lookup_n(cordic_N), .I(cavI), .Q(cavQ), .mag(cavAmplRaw), .phase(cavPhaseAdc));
	rect2pol #(.samples(samples)) cordicRef(.clk(rfClk), .ctr(ctr), .lookup_p(cordic_P), .lookup_n(cordic_N), .I(refI), .Q(refQ), .mag(refAmplRaw), .phase(refPhase));

	// calibrate device and system phases for all channels
	// calibrate device and system amplitudes for all channels, using a single multiplier block
	// device cal -> calibrate raw amplitude values to Vp at SMA connector
	// system cal -> scale amplitudes from Vp at SMA connector to system voltages at pickup
	// logic for 'locked' signal -> amplitude continuously within 0.78% of setpoint for one second
	reg [31:0] calMultInA, calMultInB;
	wire [63:0] calMultOut;
	reg [31:0] lockedCtr = 0;
	reg rfLocked = 0;
	always @ (posedge rfClk)
	begin
		fwdPhaseRaw <= fwdPhaseAdc - refPhase;
		rflPhaseRaw <= rflPhaseAdc - refPhase;
		cavPhaseRaw <= cavPhaseAdc - refPhase;
		fwdPhaseDev <= fwdPhaseRaw + fwdDevOfst;
		cavPhaseDev <= cavPhaseRaw + cavDevOfst;
		fwdPhaseSys <= fwdPhaseDev + fwdSysOfst;
		cavPhaseSys <= cavPhaseDev + cavSysOfst;
		case (ctr)
			0:
			begin
				calMultInA <= fwdAmplRaw;
				calMultInB <= fwdDevCal;
			end
			1:
			begin
				calMultInA <= rflAmplRaw;
				calMultInB <= rflDevCal;
			end
			2:
			begin
				calMultInA <= cavAmplRaw;
				calMultInB <= cavDevCal;
			end
			3:
			begin
				calMultInA <= refAmplRaw;
				calMultInB <= refDevCal;
			end
			4:
			begin
				calMultInA <= 32'h69510000;
				calMultInB <= fwdDevCal;
			end
			5:
			begin
				calMultInA <= 32'h69510000;
				calMultInB <= rflDevCal;
			end
			6:
			begin
				calMultInA <= 32'h69510000;
				calMultInB <= cavDevCal;
			end
			7:
			begin
				calMultInA <= 32'h69510000;
				calMultInB <= refDevCal;
			end
			9:
			begin
				calMultInA <= calMultOut[47:16];
				calMultInB <= fwdSysCal;
				fwdAmplDev <= |calMultOut[63:48] ? 32'hFFFFFFFF : calMultOut[47:16];
			end
			10:
			begin
				calMultInA <= calMultOut[47:16];
				calMultInB <= rflSysCal;
				rflAmplDev <= |calMultOut[63:48] ? 32'hFFFFFFFF : calMultOut[47:16];
			end
			11:
			begin
				calMultInA <= calMultOut[47:16];
				calMultInB <= cavSysCal;
				cavAmplDev <= |calMultOut[63:48] ? 32'hFFFFFFFF : calMultOut[47:16];
			end
			12:
			begin
				calMultInA <= calMultOut[47:16];
				calMultInB <= refSysCal;
				refAmplDev <= |calMultOut[63:48] ? 32'hFFFFFFFF : calMultOut[47:16];
			end
			13:
			begin
				calMultInA <= calMultOut[47:16];
				calMultInB <= fwdSysCal;
				maxFwdDev <= |calMultOut[63:48] ? 32'hFFFFFFFF : calMultOut[47:16];
			end
			14:
			begin
				calMultInA <= calMultOut[47:16];
				calMultInB <= rflSysCal;
				maxRflDev <= |calMultOut[63:48] ? 32'hFFFFFFFF : calMultOut[47:16];
			end
			15:
			begin
				calMultInA <= calMultOut[47:16];
				calMultInB <= cavSysCal;
				maxCavDev <= |calMultOut[63:48] ? 32'hFFFFFFFF : calMultOut[47:16];
			end
			16:
			begin
				calMultInA <= calMultOut[47:16];
				calMultInB <= refSysCal;
				maxRefDev <= |calMultOut[63:48] ? 32'hFFFFFFFF : calMultOut[47:16];
			end
			18:
			begin
				calMultInA <= 0;
				calMultInB <= 0;
				fwdAmplSys <= |calMultOut[63:56] ? 32'hFFFFFFFF : calMultOut[55:24];
			end
			19:
			begin
				calMultInA <= 0;
				calMultInB <= 0;
				rflAmplSys <= |calMultOut[63:56] ? 32'hFFFFFFFF : calMultOut[55:24];
			end
			20:
			begin
				calMultInA <= 0;
				calMultInB <= 0;
				cavAmplSys <= |calMultOut[63:56] ? 32'hFFFFFFFF : calMultOut[55:24];
			end
			21:
			begin
				calMultInA <= 0;
				calMultInB <= 0;
				refAmplSys <= |calMultOut[63:56] ? 32'hFFFFFFFF : calMultOut[55:24];
			end
			22:
			begin
				calMultInA <= 0;
				calMultInB <= 0;
				maxFwdSys <= |calMultOut[63:56] ? 32'hFFFFFFFF : calMultOut[55:24];
			end
			23:
			begin
				calMultInA <= 0;
				calMultInB <= 0;
				maxRflSys <= |calMultOut[63:56] ? 32'hFFFFFFFF : calMultOut[55:24];
			end
			24:
			begin
				calMultInA <= 0;
				calMultInB <= 0;
				maxCavSys <= |calMultOut[63:56] ? 32'hFFFFFFFF : calMultOut[55:24];
			end
			25:
			begin
				calMultInA <= 0;
				calMultInB <= 0;
				maxRefSys <= |calMultOut[63:56] ? 32'hFFFFFFFF : calMultOut[55:24];
			end
			default:
			begin
				calMultInA <= 0;
				calMultInB <= 0;
			end
		endcase
		if ((cavAmplSys > setAmpl + setAmpl[31:7]) | (cavAmplSys < setAmpl - setAmpl[31:7]) | ~rfOn)
			lockedCtr <= adcClkRate;
		else if (|lockedCtr)
			lockedCtr <= lockedCtr - 1;
		rfLocked <= ~|lockedCtr;
	end
	multU32U32_8cycle multCal (.clk(rfClk), .a(calMultInA), .b(calMultInB), .p(calMultOut));
	
	// convert setpoints and feedback from phase/amplitude to I/Q
	wire [32:0] setI, setQ, fdbkI, fdbkQ, findHomeI, findHomeQ, initialI, initialQ;
  reg [31:0] setIsat, setQsat, fdbkIsat, fdbkQsat, findHomeIsat, findHomeQsat, initialIsat, initialQsat;
	pol2rect #(.samples(samples), .outbits(32)) cordicSet(.clk(rfClk), .ctr(ctr), .lookup_p(cordic_P), .lookup_n(cordic_N),
		.mag(rfSetAmpl), .phase(rfSetPhase), .I(setI), .Q(setQ));
	pol2rect #(.samples(samples), .outbits(32)) cordicFdbk(.clk(rfClk), .ctr(ctr), .lookup_p(cordic_P), .lookup_n(cordic_N),
		.mag(cavAmplSys), .phase(cavPhaseSys), .I(fdbkI), .Q(fdbkQ));
	pol2rect #(.samples(samples), .outbits(32)) cordicFH(.clk(rfClk), .ctr(ctr), .lookup_p(cordic_P), .lookup_n(cordic_N),
		.mag(findHomeAmpl), .phase(rfSetPhase), .I(findHomeI), .Q(findHomeQ));
	pol2rect #(.samples(samples), .outbits(32)) cordicInit(.clk(rfClk), .ctr(ctr), .lookup_p(cordic_P), .lookup_n(cordic_N),
		.mag(initialAmpl), .phase(rfSetPhase), .I(initialI), .Q(initialQ));
    
  always @ (posedge rfClk)
  begin
    setIsat <= (setI[32] == setI[31]) ? setI[31:0] : (setI[32] ? 32'h80000001 : 32'h7FFFFFFF);
    setQsat <= (setQ[32] == setQ[31]) ? setQ[31:0] : (setQ[32] ? 32'h80000001 : 32'h7FFFFFFF);
    fdbkIsat <= (fdbkI[32] == fdbkI[31]) ? fdbkI[31:0] : (fdbkI[32] ? 32'h80000001 : 32'h7FFFFFFF);
    fdbkQsat <= (fdbkQ[32] == fdbkQ[31]) ? fdbkQ[31:0] : (fdbkQ[32] ? 32'h80000001 : 32'h7FFFFFFF);
    findHomeIsat <= (findHomeI[32] == findHomeI[31]) ? findHomeI[31:0] : (findHomeI[32] ? 32'h80000001 : 32'h7FFFFFFF);
    findHomeQsat <= (findHomeQ[32] == findHomeQ[31]) ? findHomeQ[31:0] : (findHomeQ[32] ? 32'h80000001 : 32'h7FFFFFFF);
    initialIsat <= (initialI[32] == initialI[31]) ? initialI[31:0] : (initialI[32] ? 32'h80000001 : 32'h7FFFFFFF);
    initialQsat <= (initialQ[32] == initialQ[31]) ? initialQ[31:0] : (initialQ[32] ? 32'h80000001 : 32'h7FFFFFFF);
  end
	
	// control block
	wire [31:0] attenGain, feedFwdGain2;
	wire [15:0] overflow, shiftOut;
	wire signed [31:0] ctrlI, ctrlQ;
	atten_lookup_table feedFwdLookup(.clka(Bus2IP_Clk), .addra(atten), .douta(attenGain));
	multU32U32_8cycle multFeedFwd (.clk(Bus2IP_Clk), .a(attenGain), .b(feedFwdGain), .p({overflow, feedFwdGain2, shiftOut}));
	rfCtrl rfControl(.clk(rfClk), .ctr(ctr), .rfLoopback(rfLoopback), .rfOn(rfOn), .ctrlEn(ctrlEn), .findingHome(findingHome),
		.findHomeI(findHomeIsat), .findHomeQ(findHomeQsat), .initialTime(initialTime), .rampRate(rampRate),
		.initialI(initialI), .initialQ(initialQ), .cavI(fdbkIsat), .cavQ(fdbkQsat), .setI(setIsat), .setQ(setQsat), .feedFwdGain(feedFwdGain2),
		.initialState(rfInitial), .rampState(rfRamp), .ctrlI(ctrlI), .ctrlQ(ctrlQ));
	always @ (posedge rfClk)
	begin
		rfSetPhase1 <= setPhase;
		rfSetPhase2 <= rfSetPhase1;
		if (rfSetPhase2 == rfSetPhase1)
			rfSetPhase <= rfSetPhase2;
		rfSetAmpl1 <= setAmpl;
		rfSetAmpl2 <= rfSetAmpl1;
		if (rfSetAmpl2 == rfSetAmpl1)
			rfSetAmpl <= rfSetAmpl2;
	end

	// convert control output to phase/amplitude to add in feedfwdphase, then back to I/Q for DAC
	wire [13:0] outI, outQ;
	reg signed [31:0] outPhase = 0;
	reg [31:0] outAmpl = 0;
	wire [31:0] outSat = 32'h4DB00000;//32'h4DBA76D3;
	always @ (posedge rfClk)
	begin
		outPhase <= rfLoopback ? 0 : ctrlPhase + feedFwdPhase - startupPhase;
		outAmpl <= (ctrlAmpl > outSat) ? outSat : ctrlAmpl;
	end
	rect2pol #(.samples(samples)) cordicOut1(.clk(rfClk), .ctr(ctr), .lookup_p(cordic_P), .lookup_n(cordic_N),
		.I(ctrlI), .Q(ctrlQ), .mag(ctrlAmpl), .phase(ctrlPhase));
	pol2rect #(.samples(samples), .outbits(14)) cordicOut2(.clk(rfClk), .ctr(ctr), .lookup_p(cordic_P), .lookup_n(cordic_N),
		.mag(outAmpl), .phase(outPhase), .I(outI), .Q(outQ));

	wire [35:0] csCtrl;
	wire [255:0] csData;
	chipscope_icon(.CONTROL0(csCtrl));
	chipscope_ila(.CONTROL(csCtrl), .CLK(rfClk), .TRIG0(csData));
	assign csData[15:0] = setI[31:16];
	assign csData[31:16] = setQ[31:16];
	assign csData[47:32] = cavAmplSys[31:16];
	assign csData[63:48] = cavPhaseSys[31:16];
	assign csData[79:64] = fdbkI[31:16];
	assign csData[95:80] = fdbkQ[31:16];
	assign csData[111:96] = ctrlI[31:16];
	assign csData[127:112] = ctrlQ[31:16];
	assign csData[143:128] = ctrlAmpl[31:16];
	assign csData[159:144] = ctrlPhase[31:16];
	assign csData[175:160] = outAmpl[31:16];
	assign csData[191:176] = outPhase[31:16];
	assign csData[207:192] = {outI, 2'h0};
	assign csData[223:208] = {outQ, 2'h0};
	assign csData[239:224] = outSat[31:16];
	assign csData[240] = rfOn;
	assign csData[255:241] = 0;

	// DAC clock logic
	wire dacClk;
	IBUFGDS #(.DIFF_TERM("TRUE"), .IOSTANDARD("LVDS_33")) dacClk_ibufgds (.O(dacClk), .I(CLK_DAC_P), .IB(CLK_DAC_N));
	
	// DAC output driver
	reg iqValid = 0;
	always @ (posedge rfClk)
		iqValid <= ^ctr[4:3];
	dacCtrl dacDriver(.dacClk(dacClk),.I({~outI[13], outI[12:0]}), .Q({~outQ[13], outQ[12:0]}),
		.valid(iqValid), .dacData_N(DAC_N), .dacData_P(DAC_P));
		
	// Interlock logic
	rfIlk interlocks(.clk(rfClk), .rfLoopback(rfLoopback), .extIlkOk(OUTPUT_EN), .pll0Locked(pll0locked), .pll1Locked(pll1locked), .tunerFault(1'b0),
		.rfSetOn(rfSetOn), .findingHome(findingHome), .ilkRst(ilkReset), .ilkCtrRst(ilkCtrReset), .dacSleep(DAC_SLEEP), .setRfOff(setRfOff),
		.ilkd(ilkd), .rfOn(rfOn), .ilkTurnsOffRf(ilkTurnsOffRf), .ilkResetSel(ilkResetSel),
		.fastResetTime(fastResetTime), .slowResetTime(slowResetTime), .ilkCtrs(ilkCtrs), .ilkSts(ilkSts),
		.ilkLatches(ilkLatches), .firstIlk(firstIlk), .fwdAmpl(fwdAmplSys), .rflAmpl(rflAmplSys), .cavAmpl(cavAmplSys),
		.cavLowAmpl(cavLowAmpl), .cavLowTime(cavLowTime), .cavLowMinFwd(cavLowMinFwd), .fwdRflMinRfl(fwdRflMinRfl),
		.fwdRflRatio1(findingHome ? initFwdRflRatio : fwdRflRatio1), .fwdRflTime1(rfInitial ? initFwdRflTime : fwdRflTime1),
		.fwdRflRatio2(findingHome ? initFwdRflRatio : fwdRflRatio2), .fwdRflTime2(rfInitial ? initFwdRflTime : fwdRflTime2),
		.dvdtRate(dvdtRate), .ilkRateMask(ilkRateMask), .ilkRateShift(ilkRateShift), .ilkRateLimit(ilkRateLimit));
		
	// Tuner logic
	reg [31:0] tunerError = 0;
	reg [31:0] tunerHomeCtr = 0;
	reg [2:0] tunerSetHomePulse = 0;
	reg rfOnPrev = 0;
	always @ (posedge rfClk)
	begin
		rfOnPrev <= rfOn;
		if (rfSetOn)
			tunerHomeCtr <= tunerHomeDelay;
		else if (|tunerHomeCtr)
			tunerHomeCtr <= tunerHomeCtr - 1;
		tunerGoHome <= ~|tunerHomeCtr[31:3] & |tunerHomeCtr[2:0];
		tunerError <= fwdPhaseSys - cavPhaseSys - fwdCavPhase;
		
		if (~rfOn & tunerEn)
			findingHome <= 1;
		else if (~tunerEn | (tunerError[31] & (~tunerError < tunerDeadZone)) | (~tunerError[31] & (tunerError < tunerDeadZone)))
			findingHome <= 0;		

		if (~rfOn | ~tunerEn)
		begin
			tunerDn <= 0;
			tunerUp <= 0;
		end
		else
		begin
			tunerDn <= findingHome ? tunerError[31] : tunerError[31] & (~tunerError > tunerDeadZone);
			tunerUp <= findingHome ? ~tunerError[31] : ~tunerError[31] & (tunerError > tunerDeadZone);
		end
			
		// If tuner is off, reset all control signals and states
		if (~tunerEn | ~rfOn)
			{tunerSetHomePulse, tunerSetHome} <= {1'b0, tunerSetHomePulse};
		// tuner is attempting to find and set cold home position
		else if (findingHome)
		begin
			if ((tunerError[31] & (~tunerError < tunerDeadZone)) | (~tunerError[31] & (tunerError < tunerDeadZone)))
				{tunerSetHomePulse, tunerSetHome} <= 4'hF;
			else
				{tunerSetHomePulse, tunerSetHome} <= {1'b0, tunerSetHomePulse};
		end
		// normal tuner operation
		else
			{tunerSetHomePulse, tunerSetHome} <= {1'b0, tunerSetHomePulse};
	end

	// Front panel LED status
	ledDriver(.clk(CLK12_5), .pllFault(~configDone | ~pll0locked | ~pll1locked),
		.on(rfOn), .init(rfInitial), .ramp(rfRamp),
		.closed(ctrlEn), .vswr(ilkLatches[1] | ilkLatches[2]), .dvdt(ilkLatches[3]), .low(ilkLatches[0]),
		.tnrMoving(tunerMoving), .tnrFault(tunerFault),
		.ledClk(LEDS_CLK), .ledLatch(LEDS_LATCH), .ledData(LEDS_DATA));
	
	// Front panel "RF On / Off" BNC connector
	assign RF_ON_MON = rfOn;
	assign PLC_RFON = rfOn;
	assign PLC_RFLOCKED = rfLocked;
	
	// Outputs that control the RF switches
	// When changing back to normal mode, the switch output is delayed to prevent an RF spike from being sent to the amplifier
	reg [9:0] rfLoopbackCtr = 10'h3FF;
	reg rfLoopbackDly = 1;
	always @ (posedge Bus2IP_Clk)
	begin
		rfLoopbackDly <= |rfLoopbackCtr;
		if (rfLoopback)				rfLoopbackCtr <= 10'h3FF;
		else if (|rfLoopbackCtr)	rfLoopbackCtr <= rfLoopbackCtr - 1;
	end
	assign SW_LOOPBACK_N = ~rfLoopbackDly;
	assign SW_LOOPBACK_P = rfLoopback;
	
	// Front panel analog outputs
	spiDac analogs(.clk(CLK12_5), .chan0(16'h8000), .chan1(cavAmplRaw[31:16]), .chan2(rflAmplRaw[31:16]),
		.chan3(fwdAmplRaw[31:16]), .spiCS_N(SPIDAC_CS), .spiClk(SPIDAC_CLK), .spiMosi(SPIDAC_DATA));
endmodule
