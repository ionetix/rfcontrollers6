`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Engineer: 		Nathan Usher
// Create Date:    17:51:05 03/29/2014
// Module Name:    dacCtrl
// Description: 	RF output DAC driver
// Additional Comments: 
//		
//////////////////////////////////////////////////////////////////////////////////
module dacCtrl(
	input dacClk,
	input [13:0] I,
	input [13:0] Q,
	input valid,
	output [13:0] dacData_N,
	output [13:0] dacData_P
);

	reg [1:0] ctr = 0;
	reg [1:0] ctrPrev = 0;
	reg [13:0] dacI = 14'h2000, dacQ = 14'h2000;
	reg [13:0] dacReg = 14'h2000, dacRegNext = 14'h2000;
  reg validReg = 0;

	always @ (posedge dacClk)
	begin
    validReg <= valid;
		ctr <= ctr + 1'b1;
		ctrPrev <= ctr;
		if (validReg)
		begin
			dacI <= I;
			dacQ <= Q;
		end
		dacRegNext <= ctr[0] ? dacQ : dacI;
		dacReg <= ctrPrev[1] ? ~dacRegNext : dacRegNext;
	end
	
	wire [13:0] dacDataToOBUF;
	genvar x;
	generate
		for (x=0; x<14; x=x+1)
		begin : dacDataLoop
			ODDR2 #(.DDR_ALIGNMENT("NONE"), .INIT(1'b0), .SRTYPE("SYNC")) dacData_oddr2 (.Q(dacDataToOBUF[x]),
				.C0(dacClk), .C1(1'b0), .CE(1'b1), .D0(dacReg[x]), .D1(dacReg[x]), .R(1'b0), .S(1'b0));
			OBUFDS #(.IOSTANDARD("LVDS_33")) dacData_obufds (.O(dacData_P[x]), .OB(dacData_N[x]), .I(dacDataToOBUF[x]));
		end
	endgenerate
endmodule
