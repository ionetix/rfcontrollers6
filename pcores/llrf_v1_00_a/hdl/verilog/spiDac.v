`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 		FRIB
// Engineer: 		Nathan Usher
// 
// Create Date:    11:24:34 07/11/2011 
// Design Name: 	FRIB LLRF PED2 - Analog PCB
// Module Name: 	dcmWrapper
// Project Name: 	FRIB LLRF PED2
// Description: 	Module that continuously polls the DAC setpoint registers and
//						updates the DAC when any setpoints have changed
//
// Revision: 
// Revision 0.01 - File Created
// Additional Comments: 
//		clk frequency must be <= 24MHz
//		data is expected to be unsigned
//////////////////////////////////////////////////////////////////////////////////
module spiDac(clk, chan0, chan1, chan2, chan3, spiCS_N, spiClk, spiMosi);
	input clk;
	input [15:0] chan0;
	input [15:0] chan1;
	input [15:0] chan2;
	input [15:0] chan3;
	output reg spiCS_N = 1;
	output reg spiClk = 0;
	output reg spiMosi = 0;

	reg [1:0] state = 0;
	reg [1:0] channel = 0;
	reg [4:0] ctr = 0;
	reg [23:0] shift = 24'hD00000;
	reg [15:0] chan0Prev = 0;
	reg [15:0] chan1Prev = 0;
	reg [15:0] chan2Prev = 0;
	reg [15:0] chan3Prev = 0;
	reg [15:0] data;
	wire [3:0] update;
	reg posEdge = 0, negEdge = 1;

	assign update[0] = chan0 != chan0Prev;
	assign update[1] = chan1 != chan1Prev;
	assign update[2] = chan2 != chan2Prev;
	assign update[3] = chan3 != chan3Prev;
	
	always @*
	begin
		case (channel)
			0: data <= chan0;
			1: data <= chan1;
			2: data <= chan2;
			3: data <= chan3;
		endcase
	end

	always @ (posedge clk)
	begin
		posEdge <= ~posEdge;
		negEdge <= posEdge;
		if (posEdge)
		begin
			case (state)
				// If a channel has changed, transition to the next state to send the new value.
				// Otherwise, check the next channel
				0:
				begin
					spiClk <= 0;
					spiMosi <= 0;
					spiCS_N <= 1;
					channel <= channel + 1;
					ctr <= 0;
					shift <= {5'h1A, channel, 1'h0, data};
					if (update[channel])
					begin
						state <= state + 1;
						case (channel)
							0: chan0Prev <= data;
							1: chan1Prev <= data;
							2: chan2Prev <= data;
							3: chan3Prev <= data;
						endcase
					end
				end
				// Shift out the new value
				1:
				begin
					spiClk <= 1;
					spiMosi <= shift[23];
					spiCS_N <= 0;
					ctr <= ctr + 1;
					shift <= {shift[22:0], 1'b0};
					if (ctr == 24)
						state <= state + 1;
				end
				// 2 extra delay cycles are necessary for DAC to finish conversion
				default:
				begin
					spiClk <= 0;
					spiMosi <= 0;
					spiCS_N <= 1;
					ctr <= 0;
					shift <= {5'h1A, channel, 1'h0, data};
					state <= state + 1;
				end
			endcase
		end
		if (negEdge)
			spiClk <= 0;
	end
endmodule
