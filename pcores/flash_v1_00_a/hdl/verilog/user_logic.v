//----------------------------------------------------------------------------
// user_logic.v - module
//----------------------------------------------------------------------------
//
// ***************************************************************************
// ** Copyright (c) 1995-2012 Xilinx, Inc.  All rights reserved.            **
// **                                                                       **
// ** Xilinx, Inc.                                                          **
// ** XILINX IS PROVIDING THIS DESIGN, CODE, OR INFORMATION "AS IS"         **
// ** AS A COURTESY TO YOU, SOLELY FOR USE IN DEVELOPING PROGRAMS AND       **
// ** SOLUTIONS FOR XILINX DEVICES.  BY PROVIDING THIS DESIGN, CODE,        **
// ** OR INFORMATION AS ONE POSSIBLE IMPLEMENTATION OF THIS FEATURE,        **
// ** APPLICATION OR STANDARD, XILINX IS MAKING NO REPRESENTATION           **
// ** THAT THIS IMPLEMENTATION IS FREE FROM ANY CLAIMS OF INFRINGEMENT,     **
// ** AND YOU ARE RESPONSIBLE FOR OBTAINING ANY RIGHTS YOU MAY REQUIRE      **
// ** FOR YOUR IMPLEMENTATION.  XILINX EXPRESSLY DISCLAIMS ANY              **
// ** WARRANTY WHATSOEVER WITH RESPECT TO THE ADEQUACY OF THE               **
// ** IMPLEMENTATION, INCLUDING BUT NOT LIMITED TO ANY WARRANTIES OR        **
// ** REPRESENTATIONS THAT THIS IMPLEMENTATION IS FREE FROM CLAIMS OF       **
// ** INFRINGEMENT, IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS       **
// ** FOR A PARTICULAR PURPOSE.                                             **
// **                                                                       **
// ***************************************************************************
//
//----------------------------------------------------------------------------
// Filename:          user_logic.v
// Version:           1.00.a
// Description:       User logic module.
// Date:              Sun Dec 07 00:47:58 2014 (by Create and Import Peripheral Wizard)
// Verilog Standard:  Verilog-2001
//----------------------------------------------------------------------------
// Naming Conventions:
//   active low signals:                    "*_n"
//   clock signals:                         "clk", "clk_div#", "clk_#x"
//   reset signals:                         "rst", "rst_n"
//   generics:                              "C_*"
//   user defined types:                    "*_TYPE"
//   state machine next state:              "*_ns"
//   state machine current state:           "*_cs"
//   combinatorial signals:                 "*_com"
//   pipelined or register delay signals:   "*_d#"
//   counter signals:                       "*cnt*"
//   clock enable signals:                  "*_ce"
//   internal version of output port:       "*_i"
//   device pins:                           "*_pin"
//   ports:                                 "- Names begin with Uppercase"
//   processes:                             "*_PROCESS"
//   component instantiations:              "<ENTITY_>I_<#|FUNC>"
//----------------------------------------------------------------------------

module user_logic
(
  // -- ADD USER PORTS BELOW THIS LINE ---------------
  clk,
  cs_n,
  mosi,
  miso,
  wp_n,
  hold_n,
  // -- ADD USER PORTS ABOVE THIS LINE ---------------

  // -- DO NOT EDIT BELOW THIS LINE ------------------
  // -- Bus protocol ports, do not add to or delete 
  Bus2IP_Clk,                     // Bus to IP clock
  Bus2IP_Resetn,                  // Bus to IP reset
  Bus2IP_Addr,                    // Bus to IP address bus
  Bus2IP_CS,                      // Bus to IP chip select for user logic memory selection
  Bus2IP_RNW,                     // Bus to IP read/not write
  Bus2IP_Data,                    // Bus to IP data bus
  Bus2IP_BE,                      // Bus to IP byte enables
  Bus2IP_RdCE,                    // Bus to IP read chip enable
  Bus2IP_WrCE,                    // Bus to IP write chip enable
  Bus2IP_Burst,                   // Bus to IP burst-mode qualifier
  Bus2IP_BurstLength,             // Bus to IP burst length
  Bus2IP_RdReq,                   // Bus to IP read request
  Bus2IP_WrReq,                   // Bus to IP write request
  Type_of_xfer,                   // Transfer Type
  IP2Bus_AddrAck,                 // IP to Bus address acknowledgement
  IP2Bus_Data,                    // IP to Bus data bus
  IP2Bus_RdAck,                   // IP to Bus read transfer acknowledgement
  IP2Bus_WrAck,                   // IP to Bus write transfer acknowledgement
  IP2Bus_Error                    // IP to Bus error response
  // -- DO NOT EDIT ABOVE THIS LINE ------------------
); // user_logic

// -- ADD USER PARAMETERS BELOW THIS LINE ------------
// --USER parameters added here 
// -- ADD USER PARAMETERS ABOVE THIS LINE ------------

// -- DO NOT EDIT BELOW THIS LINE --------------------
// -- Bus protocol parameters, do not add to or delete
parameter C_SLV_AWIDTH                   = 32;
parameter C_SLV_DWIDTH                   = 32;
parameter C_NUM_MEM                      = 1;
// -- DO NOT EDIT ABOVE THIS LINE --------------------

// -- ADD USER PORTS BELOW THIS LINE -----------------
output reg                                clk = 0;
output reg                                cs_n = 1;
output reg                                mosi = 0;
input                                     miso;
output                                    wp_n;
output                                    hold_n;
// -- ADD USER PORTS ABOVE THIS LINE -----------------

// -- DO NOT EDIT BELOW THIS LINE --------------------
// -- Bus protocol ports, do not add to or delete
input                                     Bus2IP_Clk;
input                                     Bus2IP_Resetn;
input      [C_SLV_AWIDTH-1 : 0]           Bus2IP_Addr;
input      [C_NUM_MEM-1 : 0]              Bus2IP_CS;
input                                     Bus2IP_RNW;
input      [C_SLV_DWIDTH-1 : 0]           Bus2IP_Data;
input      [C_SLV_DWIDTH/8-1 : 0]         Bus2IP_BE;
input      [C_NUM_MEM-1 : 0]              Bus2IP_RdCE;
input      [C_NUM_MEM-1 : 0]              Bus2IP_WrCE;
input                                     Bus2IP_Burst;
input      [7 : 0]                        Bus2IP_BurstLength;
input                                     Bus2IP_RdReq;
input                                     Bus2IP_WrReq;
input                                     Type_of_xfer;
output                                    IP2Bus_AddrAck;
output reg [C_SLV_DWIDTH-1 : 0]           IP2Bus_Data;
output                                    IP2Bus_RdAck;
output                                    IP2Bus_WrAck;
output                                    IP2Bus_Error;
// -- DO NOT EDIT ABOVE THIS LINE --------------------

//----------------------------------------------------------------------------
// Implementation
//----------------------------------------------------------------------------

	assign wp_n = 1;
	assign hold_n = 1;

	assign IP2Bus_AddrAck = Bus2IP_CS[0];
	assign IP2Bus_RdAck = Bus2IP_CS[0] & Bus2IP_RNW;
	assign IP2Bus_WrAck = Bus2IP_CS[0] & ~Bus2IP_RNW;
	assign IP2Bus_Error = 0;

	reg writeFifoReset = 0;
	reg writeFifoRdEn = 0;
	wire [31:0] writeData;
	wire writeFifoFull, writeFifoEmpty, writeFifoValid;
	
	reg readFifoReset = 0;
	reg readFifoWrEn = 0;
	reg readFifoWrEnDly = 0;
	wire [31:0] readData;
	wire readFifoFull, readFifoEmpty, readFifoValid;

	reg [15:0] writeBytes = 0, readDelayBytes = 0;
	reg [2:0] bitCtr = 0;
	reg [15:0] writeByteCtr = 0, readByteCtr = 0;
	reg [31:0] writeShifter = 0, readShifter = 0;
	reg [3:0] state = 0;
	
	reg misoReg = 0;

	wire busy;
	assign busy = |state;

	always @*
	begin
		case (Bus2IP_Addr[3:2])
			2'b00: IP2Bus_Data = {17'h0, readFifoFull, readFifoEmpty, readFifoValid, 1'b0, writeFifoFull, writeFifoEmpty, writeFifoValid, 7'h0, busy};
			2'b11: IP2Bus_Data = readData;
			default: IP2Bus_Data = 32'hFFFFFFFF;
		endcase
	end

	always @ (posedge Bus2IP_Clk)
	begin
		misoReg <= miso;
		readFifoWrEnDly <= readFifoWrEn;		// delay fifo write enable signal by 1 clock cycle to get timing right
		
		if (IP2Bus_WrAck & Bus2IP_BE[0] & (Bus2IP_Addr[3:2] == 2'b00))
		begin
			writeFifoReset <= Bus2IP_Data[1];
			readFifoReset <= Bus2IP_Data[2];
		end
		else
		begin
			writeFifoReset <= 0;
			readFifoReset <= 0;
		end
		
		case (state)
			// wait for start command from AXI bus
			4'h0:
			begin
				clk <= 0;
				cs_n <= 1;
				mosi <= 0;
				bitCtr <= 0;
				writeByteCtr <= 0;
				readByteCtr <= 0;
				readShifter <= 0;
				readFifoWrEn <= 0;
				if (IP2Bus_WrAck & |Bus2IP_BE & (Bus2IP_Addr[3:2] == 2'b01))
				begin
					state <= state + 1;
					writeBytes <= writeData[15:0];
					readDelayBytes <= writeData[31:16];
					writeFifoRdEn <= 1;
				end
				else
					writeFifoRdEn <= 0;
			end
			// delay state
			1:
			begin
				clk <= 0;
				cs_n <= 1;
				mosi <= 0;
				state <= state + 1;
				readFifoWrEn <= 0;
				writeFifoRdEn <= 0;
			end
			// load the output shift register
			2:
			begin
				clk <= 0;
				cs_n <= 1;
				mosi <= 0;
				state <= state + 1;
				readFifoWrEn <= 0;
				writeShifter <= writeData;
				writeFifoRdEn <= 1;
			end
			// set mosi
			3:
			begin
				clk <= 0;
				cs_n <= 1;
				mosi <= writeShifter[31];
				writeShifter <= {writeShifter[30:0], 1'b0};
				state <= state + 1;
				readFifoWrEn <= 0;
				writeFifoRdEn <= 0;
			end
			// set cs_n low to start command
			4:
			begin
				clk <= 0;
				cs_n <= 0;
				mosi <= mosi;
				state <= state + 1;
				readFifoWrEn <= 0;
				writeFifoRdEn <= 0;
			end
			// shift out command and write data, shift read data in
			5:
			begin
				clk <= ~clk;
				cs_n <= 0;
				if (clk)
				begin
					mosi <= writeShifter[31];
					if (&bitCtr & &writeByteCtr[1:0])
					begin
						writeShifter <= writeData;
						writeFifoRdEn <= 1;
					end
					else
					begin
						writeShifter <= {writeShifter[30:0], 1'b0};
						writeFifoRdEn <= 0;
					end
					if (~|bitCtr & (writeByteCtr == writeBytes))
					begin
						state <= state + 1;
						readFifoWrEn <= writeBytes > readDelayBytes;
					end
					else
						readFifoWrEn <= ~|bitCtr & ~|readByteCtr[1:0] & |readByteCtr[15:2];
					if (writeByteCtr >= readDelayBytes)
					begin
						readShifter <= {readShifter[30:0], misoReg};
						readByteCtr <= readByteCtr + &bitCtr;
					end
				end
				else
				begin
					bitCtr <= bitCtr + 1;
					writeByteCtr <= writeByteCtr + &bitCtr;
					readFifoWrEn <= 0;
					writeFifoRdEn <= 0;
				end
			end
			// set cs_n high to end command and load the output shift register
			6:
			begin
				clk <= 0;
				cs_n <= 1;
				mosi <= 0;
				bitCtr <= 0;
				writeByteCtr <= 0;
				state <= state + 1;
				readFifoWrEn <= 0;
				writeShifter <= 32'h05000000;
				writeFifoRdEn <= 0;
			end
			// set mosi
			7:
			begin
				clk <= 0;
				cs_n <= 1;
				mosi <= writeShifter[31];
				writeShifter <= {writeShifter[30:0], 1'b0};
				state <= state + 1;
				readFifoWrEn <= 0;
				writeFifoRdEn <= 0;
			end
			// set cs_n low to start busy check
			8:
			begin
				clk <= 0;
				cs_n <= 0;
				mosi <= 0;
				state <= state + 1;
				readFifoWrEn <= 0;
				writeFifoRdEn <= 0;
			end
			// shift out read status register 1 command
			9:
			begin
				clk <= ~clk;
				cs_n <= 0;
				readFifoWrEn <= 0;
				writeFifoRdEn <= 0;
				if (clk)
				begin
					mosi <= writeShifter[31];
					writeShifter <= {writeShifter[30:0], 1'b0};
				end
				else
				begin
					bitCtr <= bitCtr + 1;
					writeByteCtr <= writeByteCtr + &bitCtr;
					if (~|bitCtr & writeByteCtr[0])
						state <= state + 1;
				end
			end
			// continue polling status register until busy bit = 0
			10:
			begin
				clk <= ~clk;
				cs_n <= 0;
				mosi <= 0;
				readFifoWrEn <= 0;
				writeFifoRdEn <= 0;
				if (clk)
				begin
					if (~|bitCtr & ~misoReg)
						state <= state + 1;
				end
				else
					bitCtr <= bitCtr + 1;
			end
			// set cs_n high after flash chip is finished
			11:
			begin
				clk <= 0;		// make sure clk is low when entering next state
				cs_n <= 1;
				mosi <= 0;
				state <= state + 1;
				readFifoWrEn <= 0;
				writeFifoRdEn <= 0;
			end
			// default state; jump back to state 0
			default:
			begin
				clk <= 0;
				cs_n <= 1;
				mosi <= 0;
				state <= 0;
				readFifoWrEn <= 0;
				writeFifoRdEn <= 0;
			end
		endcase
	end

	flash_FIFO writeFifo (.clk(Bus2IP_Clk), .srst(writeFifoReset), .din(Bus2IP_Data),
		.wr_en(Bus2IP_CS[0] & ~Bus2IP_RNW & &Bus2IP_BE & (Bus2IP_Addr[3:2] == 2'b10)), .rd_en(writeFifoRdEn),
		.dout(writeData), .full(writeFifoFull), .empty(writeFifoEmpty), .valid(writeFifoValid));
	flash_FIFO readFifo (.clk(Bus2IP_Clk), .srst(readFifoReset), .din(readShifter),
		.wr_en(readFifoWrEnDly), .rd_en(Bus2IP_CS[0] & Bus2IP_RNW & (Bus2IP_Addr[3:2] == 2'b11)),
		.dout(readData), .full(readFifoFull), .empty(readFifoEmpty), .valid(readFifoValid));
		
/*	wire [35:0] csCtrl;
	wire [255:0] csData;
	chipscope_icon csIcon(.CONTROL0(csCtrl));
	chipscope_ila csIla(.CONTROL(csCtrl), .CLK(Bus2IP_Clk), .TRIG0(csData));
	assign csData[31:0] = writeData;
	assign csData[63:32] = readData;
	assign csData[95:64] = writeShifter;
	assign csData[127:96] = readShifter;
	assign csData[143:128] = writeBytes;
	assign csData[159:144] = readDelayBytes;
	assign csData[175:160] = writeByteCtr;
	assign csData[191:176] = readByteCtr;
	assign csData[195:192] = state;
	assign csData[198:196] = bitCtr;
	assign csData[199] = readFifoFull;
	assign csData[200] = readFifoEmpty;
	assign csData[201] = readFifoValid;
	assign csData[202] = writeFifoFull;
	assign csData[203] = writeFifoEmpty;
	assign csData[204] = writeFifoValid;
	assign csData[205] = readFifoWrEn;
	assign csData[206] = readFifoReset;
	assign csData[207] = writeFifoRdEn;
	assign csData[208] = writeFifoReset;
	assign csData[209] = clk;
	assign csData[210] = cs_n;
	assign csData[211] = mosi;
	assign csData[212] = miso;
	assign csData[213] = wp_n;
	assign csData[214] = hold_n;
	assign csData[215] = IP2Bus_Error;
	assign csData[216] = IP2Bus_WrAck;
	assign csData[217] = IP2Bus_RdAck;
	assign csData[218] = IP2Bus_AddrAck;
	assign csData[219] = Bus2IP_CS[0];
	assign csData[223:220] = Bus2IP_BE;
	assign csData[224] = Bus2IP_RNW;
	assign csData[225] = misoReg;
	assign csData[255:226] = 0;*/
endmodule
