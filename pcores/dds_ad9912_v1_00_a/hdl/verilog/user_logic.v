//----------------------------------------------------------------------------
// user_logic.v - module
//----------------------------------------------------------------------------
//
// ***************************************************************************
// ** Copyright (c) 1995-2012 Xilinx, Inc.  All rights reserved.            **
// **                                                                       **
// ** Xilinx, Inc.                                                          **
// ** XILINX IS PROVIDING THIS DESIGN, CODE, OR INFORMATION "AS IS"         **
// ** AS A COURTESY TO YOU, SOLELY FOR USE IN DEVELOPING PROGRAMS AND       **
// ** SOLUTIONS FOR XILINX DEVICES.  BY PROVIDING THIS DESIGN, CODE,        **
// ** OR INFORMATION AS ONE POSSIBLE IMPLEMENTATION OF THIS FEATURE,        **
// ** APPLICATION OR STANDARD, XILINX IS MAKING NO REPRESENTATION           **
// ** THAT THIS IMPLEMENTATION IS FREE FROM ANY CLAIMS OF INFRINGEMENT,     **
// ** AND YOU ARE RESPONSIBLE FOR OBTAINING ANY RIGHTS YOU MAY REQUIRE      **
// ** FOR YOUR IMPLEMENTATION.  XILINX EXPRESSLY DISCLAIMS ANY              **
// ** WARRANTY WHATSOEVER WITH RESPECT TO THE ADEQUACY OF THE               **
// ** IMPLEMENTATION, INCLUDING BUT NOT LIMITED TO ANY WARRANTIES OR        **
// ** REPRESENTATIONS THAT THIS IMPLEMENTATION IS FREE FROM CLAIMS OF       **
// ** INFRINGEMENT, IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS       **
// ** FOR A PARTICULAR PURPOSE.                                             **
// **                                                                       **
// ***************************************************************************
//
//----------------------------------------------------------------------------
// Filename:          user_logic.v
// Version:           1.00.a
// Description:       User logic module.
// Date:              Thu Mar 27 21:38:00 2014 (by Create and Import Peripheral Wizard)
// Verilog Standard:  Verilog-2001
//----------------------------------------------------------------------------
// Naming Conventions:
//   active low signals:                    "*_n"
//   clock signals:                         "clk", "clk_div#", "clk_#x"
//   reset signals:                         "rst", "rst_n"
//   generics:                              "C_*"
//   user defined types:                    "*_TYPE"
//   state machine next state:              "*_ns"
//   state machine current state:           "*_cs"
//   combinatorial signals:                 "*_com"
//   pipelined or register delay signals:   "*_d#"
//   counter signals:                       "*cnt*"
//   clock enable signals:                  "*_ce"
//   internal version of output port:       "*_i"
//   device pins:                           "*_pin"
//   ports:                                 "- Names begin with Uppercase"
//   processes:                             "*_PROCESS"
//   component instantiations:              "<ENTITY_>I_<#|FUNC>"
//----------------------------------------------------------------------------

module user_logic
(
  // -- ADD USER PORTS BELOW THIS LINE ---------------
  tuneUp,
  tuneDn,
  CSB,
  SCLK,
  SDIO,
  // -- ADD USER PORTS ABOVE THIS LINE ---------------

  // -- DO NOT EDIT BELOW THIS LINE ------------------
  // -- Bus protocol ports, do not add to or delete 
  Bus2IP_Clk,                     // Bus to IP clock
  Bus2IP_Resetn,                  // Bus to IP reset
  Bus2IP_Addr,                    // Bus to IP address bus
  Bus2IP_CS,                      // Bus to IP chip select for user logic memory selection
  Bus2IP_RNW,                     // Bus to IP read/not write
  Bus2IP_Data,                    // Bus to IP data bus
  Bus2IP_BE,                      // Bus to IP byte enables
  Bus2IP_RdCE,                    // Bus to IP read chip enable
  Bus2IP_WrCE,                    // Bus to IP write chip enable
  Bus2IP_Burst,                   // Bus to IP burst-mode qualifier
  Bus2IP_BurstLength,             // Bus to IP burst length
  Bus2IP_RdReq,                   // Bus to IP read request
  Bus2IP_WrReq,                   // Bus to IP write request
  Type_of_xfer,                   // Transfer Type
  IP2Bus_AddrAck,                 // IP to Bus address acknowledgement
  IP2Bus_Data,                    // IP to Bus data bus
  IP2Bus_RdAck,                   // IP to Bus read transfer acknowledgement
  IP2Bus_WrAck,                   // IP to Bus write transfer acknowledgement
  IP2Bus_Error                    // IP to Bus error response
  // -- DO NOT EDIT ABOVE THIS LINE ------------------
); // user_logic

// -- ADD USER PARAMETERS BELOW THIS LINE ------------
// --USER parameters added here 
// -- ADD USER PARAMETERS ABOVE THIS LINE ------------

// -- DO NOT EDIT BELOW THIS LINE --------------------
// -- Bus protocol parameters, do not add to or delete
parameter C_SLV_AWIDTH                   = 32;
parameter C_SLV_DWIDTH                   = 32;
parameter C_NUM_MEM                      = 1;
// -- DO NOT EDIT ABOVE THIS LINE --------------------

// -- ADD USER PORTS BELOW THIS LINE -----------------
input                                     tuneUp;
input                                     tuneDn;
output                                    CSB;
output                                    SCLK;
output                                    SDIO;
// -- ADD USER PORTS ABOVE THIS LINE -----------------

// -- DO NOT EDIT BELOW THIS LINE --------------------
// -- Bus protocol ports, do not add to or delete
input                                     Bus2IP_Clk;
input                                     Bus2IP_Resetn;
input      [C_SLV_AWIDTH-1 : 0]           Bus2IP_Addr;
input      [C_NUM_MEM-1 : 0]              Bus2IP_CS;
input                                     Bus2IP_RNW;
input      [C_SLV_DWIDTH-1 : 0]           Bus2IP_Data;
input      [C_SLV_DWIDTH/8-1 : 0]         Bus2IP_BE;
input      [C_NUM_MEM-1 : 0]              Bus2IP_RdCE;
input      [C_NUM_MEM-1 : 0]              Bus2IP_WrCE;
input                                     Bus2IP_Burst;
input      [7 : 0]                        Bus2IP_BurstLength;
input                                     Bus2IP_RdReq;
input                                     Bus2IP_WrReq;
input                                     Type_of_xfer;
output                                    IP2Bus_AddrAck;
output     [C_SLV_DWIDTH-1 : 0]           IP2Bus_Data;
output                                    IP2Bus_RdAck;
output                                    IP2Bus_WrAck;
output                                    IP2Bus_Error;
// -- DO NOT EDIT ABOVE THIS LINE --------------------

//----------------------------------------------------------------------------
// Implementation
//----------------------------------------------------------------------------

	reg incInc = 0, decInc = 0;			// increment/decrement step size command
	reg incFreq = 0, decFreq = 0;			// increment/decrement frequency command
	reg [31:0] minFreqSet = 0;				// minimum frequency that can be set by decFreq command
	reg [31:0] maxFreqSet = 400000000;	// maximum frequency that can be set by incFreq command
	reg [31:0] freqSet = 67000000;		// start frequency when DDS is first configured
	reg [28:0] minFreq = 0;					// minimum frequency (force <= 400 MHz)
	reg [28:0] maxFreq = 400000000;		// maximum frequency (forced >= freq and <= 400 MHz)
	reg [28:0] freq = 67000000;			// frequency (in Hz)
	reg [2:0] stepIndex = 3;				// index into possible step sizes
	reg [23:0] step;							// frequency increment/decrement step size (in Hz)
	reg [47:0] ftw = 48'h170A3D909F80;	// current frequency tuning word stored in DDS
	reg resetDDS = 0;							// reset DDS state machine
	reg [3:0] resetCtr = 0;					// holds resetDDS for 16 clock cycles
	reg configDone = 0;						// configuration status

	// AXI read command
	reg [31:0] dataToBus;
	always @*
	begin
		case (Bus2IP_Addr[5:2])
			4'h0:		dataToBus <= {3'h0, freq};
			4'h1:		dataToBus <= {8'h0, step};
			4'h2:		dataToBus <= 0;
			4'h3:		dataToBus <= 0;
			4'h4:		dataToBus <= 0;
			4'h5:		dataToBus <= 0;
			4'h6:		dataToBus <= 0;
			4'h7:		dataToBus <= {31'h0, configDone};
			4'h8:		dataToBus <= {3'h0, minFreq};
			4'h9:		dataToBus <= {3'h0, maxFreq};
			default:	dataToBus <= 32'hFFFFFFFF;
		endcase
	end
	
	// AXI write command
	always @ (posedge Bus2IP_Clk)
	begin
		// set frequency step size based on index
		case (stepIndex)
			0:			step <= 1;
			1:			step <= 10;
			2:			step <= 100;
			3:			step <= 1000;
			4:			step <= 10000;
			default:	step <= 100000;
		endcase
		if (IP2Bus_WrAck & Bus2IP_BE[0] & Bus2IP_Data[0] & (Bus2IP_Addr[5:2] == 4'h6))
			resetCtr <= 1;
		else if (|resetCtr)
			resetCtr <= resetCtr + 1;
		resetDDS <= |resetCtr;
		
		// Regular registers
		if (minFreqSet > 400000000)			minFreq <= 400000000;
		else											minFreq <= minFreqSet[28:0];
		if (maxFreqSet < minFreq)				maxFreq <= minFreq;
		else if (maxFreqSet > 400000000)		maxFreq <= 400000000;
		else											maxFreq <= maxFreqSet[28:0];
		if (freqSet < minFreq)					freq <= minFreq;
		else if (freqSet > maxFreq)			freq <= maxFreq;
		else											freq <= freqSet[28:0];

		if (IP2Bus_WrAck & Bus2IP_BE[0])
		begin
			case (Bus2IP_Addr[5:2])
				4'h0: 													freqSet[7:0]		<= Bus2IP_Data[7:0];
				4'h2: if (Bus2IP_Data[0] & ~&stepIndex[2:1])	stepIndex			<= stepIndex + 1;
				4'h3: if (Bus2IP_Data[0] & |stepIndex)			stepIndex			<= stepIndex - 1;
				4'h4: if (Bus2IP_Data[0])							freqSet				<= freq + step;
				4'h5: if (Bus2IP_Data[0])							freqSet				<= freq - step;
				4'h8:														minFreqSet[7:0]	<= Bus2IP_Data[7:0];
				4'h9:														maxFreqSet[7:0]	<= Bus2IP_Data[7:0];
			endcase
		end
		if (IP2Bus_WrAck & Bus2IP_BE[1])
		begin
			case (Bus2IP_Addr[5:2])
				4'h0: freqSet[15:8]			<= Bus2IP_Data[15:8];
				4'h8:	minFreqSet[15:8]		<= Bus2IP_Data[15:8];
				4'h9:	maxFreqSet[15:8]		<= Bus2IP_Data[15:8];
			endcase
		end
		if (IP2Bus_WrAck & Bus2IP_BE[2])
		begin
			case (Bus2IP_Addr[5:2])
				4'h0: freqSet[23:16]			<= Bus2IP_Data[23:16];
				4'h8:	minFreqSet[23:16]		<= Bus2IP_Data[23:16];
				4'h9:	maxFreqSet[23:16]		<= Bus2IP_Data[23:16];
			endcase
		end
		if (IP2Bus_WrAck & Bus2IP_BE[3])
		begin
			case (Bus2IP_Addr[5:2])
				4'h0: freqSet[31:24]			<= Bus2IP_Data[31:24];
				4'h8:	minFreqSet[31:24]		<= Bus2IP_Data[31:24];
				4'h9:	maxFreqSet[31:24]		<= Bus2IP_Data[31:24];
			endcase
		end
	end
	
	// Output signals to AXI bus
	assign IP2Bus_Data = IP2Bus_RdAck ? dataToBus : 32'h0;
	assign IP2Bus_AddrAck = Bus2IP_CS[0];
	assign IP2Bus_WrAck = Bus2IP_CS[0] & ~Bus2IP_RNW;
	assign IP2Bus_RdAck = Bus2IP_CS[0] & Bus2IP_RNW;
	assign IP2Bus_Error = 0;

	// calculates the new frequency tuning word when the frequency is changed
	wire [47:0] newFtw;
	ftwMultiplier625 calcFtw (.clk(Bus2IP_Clk), .a(freq), .p(newFtw));

	// initializes the DDS and sends commands when the frequency tuning word changes
	wire valid;						// command is ready to send to DDS
	wire [47:0] cmd;				// command to send to DDS
	wire cmdDone;					// command sent to DDS, ready for next command
	serialCtrl ddsCtrl(.clk(Bus2IP_Clk), .cmdValid(valid), .cmd(cmd), .done(cmdDone),
		.sclk(SCLK), .csb(CSB),	.sdio(SDIO));

	// FIFO that buffers the data bits to the AD9912
	reg [7:0] bits = 0;		
	reg [15:0] addr = 0;
	reg [23:0] data = 0;
	reg wrEn = 0;
	serialCommandFifo cmdBuffer(.clk(Bus2IP_Clk), .din({bits, addr, data}), .wr_en(wrEn),
		.rd_en(cmdDone), .dout(cmd), .valid(valid));
		
	// state machine that generates commands for the DDS
	reg [3:0] state = 0;
	reg configuring = 0;
	reg [9:0] configDelay = 0;

	always @ (posedge Bus2IP_Clk)
	begin
		if (resetDDS)
		begin
			bits <= 0;
			addr <= 0;
			data <= 0;
			wrEn <= 0;
			state <= 1;
			configuring <= 0;
			configDelay <= 0;
		end
		else
		begin
			configuring <= valid;
			if (|configDelay | (configuring & ~valid))
				configDelay <= configDelay + 1'b1;
			configDone <= configDone | &configDelay;
			case (state)
				// Wait state (wait until software is ready to configure DDS)
				0:
				begin
					bits <= 0;
					addr <= 0;
					data <= 0;
					wrEn <= 0;
					configuring <= 0;
					configDelay <= 0;
					if (resetDDS)
						state <= state + 1;
				end
				// Reset state (return here if software resets DDS)
				1:
				begin
					bits <= 0;
					addr <= 0;
					data <= 0;
					wrEn <= 0;
					configuring <= 0;
					configDelay <= 0;
					state <= state + 1;
				end
				// Reset DDS
				2:
				begin
					bits <= 8'd24;								// total of 24 bits, including address
					addr <= {1'b0, 2'b00, 13'h0000};		// write, 1 byte of data, address 0x0000
					data <= {8'h3C, 16'h0};					// data 0x3C
					wrEn <= 1'b1;
					state <= state + 1;
				end
				// Reset bit set in previous state must be manually cleared
				3:
				begin
					bits <= 8'd24;								// total of 24 bits, including address
					addr <= {1'b0, 2'b00, 13'h0000};		// write, 1 byte of data, address 0x0000
					data <= {8'h18, 16'h0};					// data 0x18
					wrEn <= 1'b1;
					state <= state + 1;
				end
				// Reset the DDS
				4:
				begin
					bits <= 8'd24;								// total of 24 bits, including address
					addr <= {1'b0, 2'b00, 13'h0012};		// write, 1 byte of data, address 0x0012
					data <= {8'h01, 16'h0};					// data 0x01
					wrEn <= 1'b0;								// disabled - full system reset is done in previous states
					state <= state + 1;
				end
				// Set the PLL parameters register
				5:
				begin
					bits <= 8'd24;								// total of 24 bits, including address
					addr <= {1'b0, 2'b00, 13'h0022};		// write, 1 byte of data, address 0x0022
					data <= {8'h04, 16'h0};					// data 0x04
					wrEn <= 1'b0;								// disabled - should not be change unless hardware is changed
					state <= state + 1;
				end
				// Power off CMOS output (on by default)
				7:
				begin
					bits <= 8'd24;								// total of 24 bits, including address
					addr <= {1'b0, 2'b00, 13'h0010};		// write, 1 byte of data, address 0x0010
					data <= {8'h90, 16'h0};					// data 0x90 - disable PLL
					wrEn <= 1'b1;
					state <= state + 1;
				end
				// Increase output level
				8:
				begin
					bits <= 8'd24;								// total of 24 bits, including address
					addr <= {1'b0, 2'b00, 13'h040C};		// write, 1 byte of data, address 0x040C
					data <= {8'h03, 16'h0};					// data 0x03 is aligned in msb, lsb filled with 0s
					wrEn <= 1'b1;
					ftw <= newFtw;
					state <= state + 1;
				end
				// Set upper 3 bytes of tuning word
				9:
				begin
					bits <= 8'd40;								// total of 40 bits, including address
					addr <= {1'b0, 2'b10, 13'h01AB};		// write, 3 bytes of data, start in high address of 0x01AB
					data <= ftw[47:24];						// data: frequency tuning word upper half
					wrEn <= 1'b1;
					state <= state + 1;
				end
				// Set lower 3 bytes of tuning word
				10:
				begin
					bits <= 8'd40;								// total of 40 bits, including address
					addr <= {1'b0, 2'b10, 13'h01A8};		// write, 3 bytes of data, start at addres 0x01A8
					data <= ftw[23:0];						// data: frequency tuning word lower half
					wrEn <= 1'b1;
					state <= state + 1;
				end
				// do an IO update using the serial bus
				11:
				begin
					bits <= 8'd24;								// total of 24 bits, including address
					addr <= {1'b0, 2'b00, 13'h0005};		// write, 1 byte of data, address 0x0005
					data <= {8'h01, 16'h0};					// write 1 to lsb of register, fill lsb of shifter with 0s
					wrEn <= 1'b1;
					state <= state + 1;
				end
				// idle
				12:
				begin
					bits <= 0;
					addr <= 0;
					data <= 0;
					wrEn <= 0;
					if (newFtw != ftw)
						state <= state + 1;
				end
				// frequency has changed, update registers
				13:
				begin
					ftw <= newFtw;
					bits <= 0;
					addr <= 0;
					data <= 0;
					wrEn <= 0;
					state <= state + 1;
				end
				// send new frequency to DDS
				14:
				begin
					bits <= 0;
					addr <= 0;
					data <= 0;
					wrEn <= 0;
					state <= 9;
				end
				default:
				begin
					bits <= 0;
					addr <= 0;
					data <= 0;
					wrEn <= 0;
					state <= state + 1;
				end
			endcase
		end
	end
/*
	wire [35:0] csCtrl;
	wire [255:0] csData;
	chipscope_icon csIcon (.CONTROL0(csCtrl));
	chipscope_ila csIla (.CONTROL(csCtrl), .CLK(Bus2IP_Clk), .TRIG0(csData));
	assign csData[47:0] = ftw;
	assign csData[95:48] = cmd;
	assign csData[127:96] = freqSet;
	assign csData[156:128] = freq;
	assign csData[180:157] = step;
	assign csData[204:181] = data;
	assign csData[220:205] = addr;
	assign csData[230:221] = configDelay;
	assign csData[238:231] = bits;
	assign csData[242:239] = resetCtr;
	assign csData[246:243] = state;
	assign csData[247] = resetDDS;
	assign csData[248] = configDone;
	assign csData[249] = valid;
	assign csData[250] = cmdDone;
	assign csData[251] = wrEn;
	assign csData[252] = configuring;
	assign csData[253] = CSB;
	assign csData[254] = SCLK;
	assign csData[255] = SDIO;*/
endmodule
