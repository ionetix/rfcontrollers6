`timescale 1ns / 1ps
module rfConfig(
	input clk,
	input [35:0] instruction,
	input valid,
	output reg read = 0,
	output spiClk,
	output spiMosi,
	output reg spiAdcEn_N = 1,
	output reg spiPll0En_N = 0,
	output reg spiPll1En_N = 0,
	output reg bitSlipCal = 0,
	output reg pllSync_N = 1
	);

	reg [31:0] shifter;
	reg [4:0] ctr = 0;
	reg [2:0] state = 0;
	reg serialClk = 0;
	
	assign spiMosi = shifter[31];
	assign spiClk = instruction[34] ? ~serialClk : serialClk;
	reg sync_cpuClk = 0;
	(* ASYNC_REG="TRUE" *) reg [1:0] sync_refClk = 0;
	always @ (posedge clk)
	begin
		bitSlipCal <= instruction[35] & (instruction[1:0] == 2'b01);
		pllSync_N <= ~instruction[35] | (instruction[1:0] != 2'b10);
		case (state)
			// read next command from ROM
			0:
			begin
				read <= valid;
				spiAdcEn_N <= 1;
				spiPll1En_N <= 0;
				spiPll0En_N <= 0;
				shifter <= instruction[31:0];
				ctr <= 0;
				serialClk <= 0;
				if (valid)
					state <= state + 1;
			end
			// set the enable pin for the target
			1:
			begin
				read <= 0;
				spiAdcEn_N <= ~instruction[34];
				spiPll1En_N <= 0;
				spiPll0En_N <= 0;
				ctr <= 0;
				serialClk <= 0;
				state <= state + 1;
			end
			// shift data out on negative edge of serial clock - for ADC commands,
			// serial clock is inverted compared to PLL
			2:
			begin
				read <= 0;
				spiAdcEn_N <= ~instruction[34];
				spiPll1En_N <= 0;
				spiPll0En_N <= 0;
				if (serialClk)
				begin
					shifter <= {shifter[30:0], 1'b0};
					ctr <= ctr + 1;
				end
				serialClk <= ~serialClk;
				if ((instruction[34] & (ctr == 15)) | (ctr == 31))
					state <= state + 1;
			end
			// wait state before PLL LE to check if timing is a problem
			3:
			begin
				read <= 0;
				spiAdcEn_N <= 1;
				spiPll1En_N <= 0;
				spiPll0En_N <= 0;
				shifter <= instruction[31:0];
				ctr <= 0;
				serialClk <= 0;
				state <= state + 1;
			end
			// reset the enable pin for the target IC and advance to next instruction
			4:
			begin
				read <= 0;
				spiAdcEn_N <= 1;
				spiPll1En_N <= instruction[33];
				spiPll0En_N <= instruction[32];
				shifter <= instruction[31:0];
				ctr <= 0;
				serialClk <= 0;
				state <= state + 1;
			end
			// additional states to check if timing is a problem
			default:
			begin
				read <= 0;
				spiAdcEn_N <= 1;
				spiPll1En_N <= 0;
				spiPll0En_N <= 0;
				shifter <= instruction[31:0];
				ctr <= 0;
				serialClk <= 0;
				state <= state + 1;
			end
		endcase
	end
endmodule
