`timescale 1ns / 1ps
module tempSensor(
	input clk,
	output reg temp_clk = 0,
	output reg temp_cs = 0,
	input temp_data,
	output reg [31:0] temp_inst = 0,
	output reg [31:0] temp_avg = 32'hFFFFFFFF
	);

	reg [18:0] ctr = 0;
	reg [15:0] shifter = 0;
	reg [15:0] tempRaw;
	wire [31:0] tempMult;
	always @ (posedge clk)
	begin
		ctr <= ctr + 1;
		temp_clk <= ctr[0] & (ctr < 44);
		temp_cs <= (ctr > 43);
		if (ctr[0])
			shifter <= {shifter[15:0], temp_data};
		if (ctr == 44)
			tempRaw <= shifter;
		temp_inst <= tempMult - 32'h00320000;		// temp (degrees C) = 330 * RAW - 50
		if (&ctr)
			temp_avg <= &temp_avg ? temp_inst : temp_avg + (temp_inst >> 8) - (temp_avg >> 8);
	end
	tempSensorMult multBy330 (.clk(clk), .a(tempRaw), .p(tempMult));
	
/*	wire [35:0] csCtrl;
	wire [255:0] csData;
	chipscope_icon csIcon (.CONTROL0(csCtrl));
	chipscope_ila csIla (.CONTROL(csCtrl), .CLK(clk), .TRIG0(csData));
	assign csData[31:0] = temp_inst;
	assign csData[63:32] = temp_avg;
	assign csData[95:64] = tempMult;
	assign csData[111:96] = shifter;
	assign csData[127:112] = tempRaw;
	assign csData[146:128] = ctr;
	assign csData[147] = temp_clk;
	assign csData[148] = temp_cs;
	assign csData[149] = temp_data;
	assign csData[255:150] = 0;*/
endmodule
