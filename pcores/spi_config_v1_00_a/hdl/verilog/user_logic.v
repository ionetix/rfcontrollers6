//----------------------------------------------------------------------------
// user_logic.v - module
//----------------------------------------------------------------------------
//
// ***************************************************************************
// ** Copyright (c) 1995-2012 Xilinx, Inc.  All rights reserved.            **
// **                                                                       **
// ** Xilinx, Inc.                                                          **
// ** XILINX IS PROVIDING THIS DESIGN, CODE, OR INFORMATION "AS IS"         **
// ** AS A COURTESY TO YOU, SOLELY FOR USE IN DEVELOPING PROGRAMS AND       **
// ** SOLUTIONS FOR XILINX DEVICES.  BY PROVIDING THIS DESIGN, CODE,        **
// ** OR INFORMATION AS ONE POSSIBLE IMPLEMENTATION OF THIS FEATURE,        **
// ** APPLICATION OR STANDARD, XILINX IS MAKING NO REPRESENTATION           **
// ** THAT THIS IMPLEMENTATION IS FREE FROM ANY CLAIMS OF INFRINGEMENT,     **
// ** AND YOU ARE RESPONSIBLE FOR OBTAINING ANY RIGHTS YOU MAY REQUIRE      **
// ** FOR YOUR IMPLEMENTATION.  XILINX EXPRESSLY DISCLAIMS ANY              **
// ** WARRANTY WHATSOEVER WITH RESPECT TO THE ADEQUACY OF THE               **
// ** IMPLEMENTATION, INCLUDING BUT NOT LIMITED TO ANY WARRANTIES OR        **
// ** REPRESENTATIONS THAT THIS IMPLEMENTATION IS FREE FROM CLAIMS OF       **
// ** INFRINGEMENT, IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS       **
// ** FOR A PARTICULAR PURPOSE.                                             **
// **                                                                       **
// ***************************************************************************
//
//----------------------------------------------------------------------------
// Filename:          user_logic.v
// Version:           1.00.a
// Description:       User logic module.
// Date:              Tue Mar 24 14:03:38 2015 (by Create and Import Peripheral Wizard)
// Verilog Standard:  Verilog-2001
//----------------------------------------------------------------------------
// Naming Conventions:
//   active low signals:                    "*_n"
//   clock signals:                         "clk", "clk_div#", "clk_#x"
//   reset signals:                         "rst", "rst_n"
//   generics:                              "C_*"
//   user defined types:                    "*_TYPE"
//   state machine next state:              "*_ns"
//   state machine current state:           "*_cs"
//   combinatorial signals:                 "*_com"
//   pipelined or register delay signals:   "*_d#"
//   counter signals:                       "*cnt*"
//   clock enable signals:                  "*_ce"
//   internal version of output port:       "*_i"
//   device pins:                           "*_pin"
//   ports:                                 "- Names begin with Uppercase"
//   processes:                             "*_PROCESS"
//   component instantiations:              "<ENTITY_>I_<#|FUNC>"
//----------------------------------------------------------------------------

module user_logic
(
  // -- ADD USER PORTS BELOW THIS LINE ---------------
  CLK12_5,
  cpuSelfReset,
  ATTEN_CLK,
  ATTEN_DATA,
  ATTEN_LE,
  SPI_ADCen,
  SPI_CLK,
  SPI_DATA,
  SPI_PLL0en,
  SPI_PLL1en,
  bitSlipCal,
  pll0locked,
  pll1locked,
  configDone,
  pllSync_N,
  serdesCalDone,
  atten,
  temp_clk,
  temp_cs,
  temp_data,
  // -- ADD USER PORTS ABOVE THIS LINE ---------------

  // -- DO NOT EDIT BELOW THIS LINE ------------------
  // -- Bus protocol ports, do not add to or delete 
  Bus2IP_Clk,                     // Bus to IP clock
  Bus2IP_Resetn,                  // Bus to IP reset
  Bus2IP_Addr,                    // Bus to IP address bus
  Bus2IP_CS,                      // Bus to IP chip select for user logic memory selection
  Bus2IP_RNW,                     // Bus to IP read/not write
  Bus2IP_Data,                    // Bus to IP data bus
  Bus2IP_BE,                      // Bus to IP byte enables
  Bus2IP_RdCE,                    // Bus to IP read chip enable
  Bus2IP_WrCE,                    // Bus to IP write chip enable
  Bus2IP_Burst,                   // Bus to IP burst-mode qualifier
  Bus2IP_BurstLength,             // Bus to IP burst length
  Bus2IP_RdReq,                   // Bus to IP read request
  Bus2IP_WrReq,                   // Bus to IP write request
  Type_of_xfer,                   // Transfer Type
  IP2Bus_AddrAck,                 // IP to Bus address acknowledgement
  IP2Bus_Data,                    // IP to Bus data bus
  IP2Bus_RdAck,                   // IP to Bus read transfer acknowledgement
  IP2Bus_WrAck,                   // IP to Bus write transfer acknowledgement
  IP2Bus_Error                    // IP to Bus error response
  // -- DO NOT EDIT ABOVE THIS LINE ------------------
); // user_logic

// -- ADD USER PARAMETERS BELOW THIS LINE ------------
// --USER parameters added here 
// -- ADD USER PARAMETERS ABOVE THIS LINE ------------

// -- DO NOT EDIT BELOW THIS LINE --------------------
// -- Bus protocol parameters, do not add to or delete
parameter C_SLV_AWIDTH                   = 32;
parameter C_SLV_DWIDTH                   = 32;
parameter C_NUM_MEM                      = 1;
// -- DO NOT EDIT ABOVE THIS LINE --------------------

// -- ADD USER PORTS BELOW THIS LINE -----------------
input                                     CLK12_5;
output reg                                cpuSelfReset = 0;
output                                    ATTEN_CLK;
output                                    ATTEN_DATA;
output                                    ATTEN_LE;
output                                    SPI_ADCen;
output                                    SPI_CLK;
output                                    SPI_DATA;
output                                    SPI_PLL0en;
output                                    SPI_PLL1en;
output                                    bitSlipCal;
input                                     pll0locked;
input                                     pll1locked;
output                                    configDone;
output                                    pllSync_N;
input                                     serdesCalDone;
output [5:0]                              atten;
output                                    temp_clk;
output                                    temp_cs;
input                                     temp_data;
// -- ADD USER PORTS ABOVE THIS LINE -----------------

// -- DO NOT EDIT BELOW THIS LINE --------------------
// -- Bus protocol ports, do not add to or delete
input                                     Bus2IP_Clk;
input                                     Bus2IP_Resetn;
input      [C_SLV_AWIDTH-1 : 0]           Bus2IP_Addr;
input      [C_NUM_MEM-1 : 0]              Bus2IP_CS;
input                                     Bus2IP_RNW;
input      [C_SLV_DWIDTH-1 : 0]           Bus2IP_Data;
input      [C_SLV_DWIDTH/8-1 : 0]         Bus2IP_BE;
input      [C_NUM_MEM-1 : 0]              Bus2IP_RdCE;
input      [C_NUM_MEM-1 : 0]              Bus2IP_WrCE;
input                                     Bus2IP_Burst;
input      [7 : 0]                        Bus2IP_BurstLength;
input                                     Bus2IP_RdReq;
input                                     Bus2IP_WrReq;
input                                     Type_of_xfer;
output                                    IP2Bus_AddrAck;
output reg [C_SLV_DWIDTH-1 : 0]           IP2Bus_Data;
output                                    IP2Bus_RdAck;
output                                    IP2Bus_WrAck;
output                                    IP2Bus_Error;
// -- DO NOT EDIT ABOVE THIS LINE --------------------

//----------------------------------------------------------------------------
// Implementation
//----------------------------------------------------------------------------

	// additional upper bits are stored so that value can be saturated if it is set too high
	reg [16:0] attenUnsat = 17'h0003F;		// initial value 31.5 dB (until updated from system calibration)
	assign atten = |attenUnsat[16:6] ? 6'h3F : attenUnsat[5:0];
	reg softConfigDone = 0, pllUnlockReboot = 0;
	reg [15:0] systemType = 16'h0005;
	reg [7:0] selfResetCtr = 0;
	wire [31:0] temp_inst;						// most recent temperature sensor reading
	wire [31:0] temp_avg;						// averaged temperature sensor reading
	
	assign IP2Bus_WrAck = |Bus2IP_WrCE;
	assign IP2Bus_RdAck = |Bus2IP_RdCE;
	assign IP2Bus_Error = 0;

	always @*
	begin
		case (Bus2IP_Addr[5:2])
			4'h0:		IP2Bus_Data <= {11'h0, atten, 15'h0};
			4'h1:		IP2Bus_Data <= {30'h0, pll1locked, pll0locked};
			4'h2:		IP2Bus_Data <= {31'h0, serdesCalDone};
			4'h3:		IP2Bus_Data <= {28'h0, pllUnlockReboot, configDone, ~valid, softConfigDone};
			// 4'h4-4'h7 are write-only
			4'h8:		IP2Bus_Data <= {16'h0, systemType};
			4'h9:		IP2Bus_Data <= 0;		// 4'h9 -> write to this register to reset the CPU
			4'hA:		IP2Bus_Data <= temp_inst;
			4'hB:		IP2Bus_Data <= temp_avg;
			default:	IP2Bus_Data <= 32'hFFFFFFFF;
		endcase
	end
	
	always @ (posedge Bus2IP_Clk)
	begin
		if (|selfResetCtr | (IP2Bus_WrAck & Bus2IP_BE[0] & (Bus2IP_Addr[5:2] == 4'h9) & Bus2IP_Data[0]))
			selfResetCtr <= selfResetCtr + 1;
		cpuSelfReset <= |selfResetCtr;
		if (IP2Bus_WrAck & Bus2IP_BE[0])
		begin
			case (Bus2IP_Addr[5:2])
				4'h3:	{pllUnlockReboot, softConfigDone}		<= {Bus2IP_Data[3], Bus2IP_Data[0]};
				4'h8:	systemType[7:0]	<= Bus2IP_Data[7:0];
			endcase
		end
		if (IP2Bus_WrAck & Bus2IP_BE[1])
		begin
			case (Bus2IP_Addr[5:2])
				4'h0:	attenUnsat[0]		<= Bus2IP_Data[15];
				4'h8:	systemType[15:8]	<= Bus2IP_Data[15:8];
			endcase
		end
		if (IP2Bus_WrAck & Bus2IP_BE[2])
		begin
			case (Bus2IP_Addr[5:2])
				4'h0:	attenUnsat[8:1]	<= Bus2IP_Data[23:16];
			endcase
		end
		if (IP2Bus_WrAck & Bus2IP_BE[3])
		begin
			case (Bus2IP_Addr[5:2])
				4'h0:	attenUnsat[16:9]	<= Bus2IP_Data[31:24];
			endcase
		end
	end
	
	wire read;
	wire [35:0] instruction;
	wire valid;
	wire [3:0] FIFO_prefix;
	assign FIFO_prefix[0] = (Bus2IP_Addr[5:2] == 4'h4);
	assign FIFO_prefix[1] = (Bus2IP_Addr[5:2] == 4'h5);
	assign FIFO_prefix[2] = (Bus2IP_Addr[5:2] == 4'h6);
	assign FIFO_prefix[3] = (Bus2IP_Addr[5:2] == 4'h7);
	assign configDone = softConfigDone & ~valid;
	FIFO36bit configFIFO (.wr_clk(Bus2IP_Clk), .rd_clk(CLK12_5), .din({FIFO_prefix, Bus2IP_Data}),
		.wr_en((Bus2IP_Addr[5:4] == 2'b01) & IP2Bus_WrAck), .rd_en(read), .dout(instruction), .valid(valid));
	serialAttenuator outAtten(.clk(CLK12_5), .atten(atten), .sclk(ATTEN_CLK),
		.sdata(ATTEN_DATA), .le(ATTEN_LE));
	rfConfig adc_pll_config(.clk(CLK12_5), .instruction(instruction), .valid(valid), .read(read),
		.spiClk(SPI_CLK), .spiMosi(SPI_DATA), .spiAdcEn_N(SPI_ADCen), .spiPll0En_N(SPI_PLL0en),
		.spiPll1En_N(SPI_PLL1en), .bitSlipCal(bitSlipCal), .pllSync_N(pllSync_N));
	tempSensor pcb_temp(.clk(CLK12_5), .temp_clk(temp_clk), .temp_cs(temp_cs), .temp_data(temp_data),
		.temp_inst(temp_inst), .temp_avg(temp_avg));
endmodule
