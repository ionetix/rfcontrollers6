################################################################################
# Automatically-generated file. Do not edit!
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
C_SRCS += \
../src/dispatch.c \
../src/echo.c \
../src/main.c \
../src/platform.c \
../src/prot_malloc.c 

LD_SRCS += \
../src/lscript.ld 

OBJS += \
./src/dispatch.o \
./src/echo.o \
./src/main.o \
./src/platform.o \
./src/prot_malloc.o 

C_DEPS += \
./src/dispatch.d \
./src/echo.d \
./src/main.d \
./src/platform.d \
./src/prot_malloc.d 


# Each subdirectory must supply rules for building sources it contributes
src/%.o: ../src/%.c
	@echo 'Building file: $<'
	@echo 'Invoking: MicroBlaze gcc compiler'
	mb-gcc -Wall -O0 -g3 -c -fmessage-length=0 -I../../ethernet_bsp/microblaze_0/include -mlittle-endian -mxl-barrel-shift -mxl-pattern-compare -mcpu=v8.50.c -mno-xl-soft-mul -mhard-float -Wl,--no-relax -ffunction-sections -fdata-sections -MMD -MP -MF"$(@:%.o=%.d)" -MT"$(@:%.o=%.d)" -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '


