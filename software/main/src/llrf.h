/*
 * llrf.h
 *
 *  Created on: Mar 24, 2015
 *      Author: nusher
 */

#ifndef LLRF_H_
#define LLRF_H_

#include "regMap.h"

int stepper_busy();
void sync_pll_ctrs();
void sync_adc_ctrs();
void init_stepper(u32 systemType);
void init_dds(u32 systemType);
int init_pll0(u32 systemType);
int init_pll1(u32 systemType);
void init_adc();
void sync();
void calc_feedFwd();
void load_devcal();
void load_syscal();
void load_settings();

#endif /* LLRF_H_ */
