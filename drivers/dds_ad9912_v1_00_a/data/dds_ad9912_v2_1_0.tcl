##############################################################################
## Filename:          Y:\Documents\ionetix\firmware\LLRFv1XPS/drivers/dds_ad9912_v1_00_a/data/dds_ad9912_v2_1_0.tcl
## Description:       Microprocess Driver Command (tcl)
## Date:              Thu Mar 27 21:38:02 2014 (by Create and Import Peripheral Wizard)
##############################################################################

#uses "xillib.tcl"

proc generate {drv_handle} {
  xdefine_include_file $drv_handle "xparameters.h" "dds_ad9912" "NUM_INSTANCES" "DEVICE_ID" "C_S_AXI_MEM0_BASEADDR" "C_S_AXI_MEM0_HIGHADDR" 
}
