/*****************************************************************************
* Filename:          Y:\Documents\ionetix\firmware\LLRFv1XPS/drivers/dds_ad9912_v1_00_a/src/dds_ad9912.h
* Version:           1.00.a
* Description:       dds_ad9912 Driver Header File
* Date:              Thu Mar 27 21:38:02 2014 (by Create and Import Peripheral Wizard)
*****************************************************************************/

#ifndef DDS_AD9912_H
#define DDS_AD9912_H

/***************************** Include Files *******************************/

#include "xbasic_types.h"
#include "xstatus.h"
#include "xil_io.h"

/************************** Constant Definitions ***************************/


/**************************** Type Definitions *****************************/


/**
 *
 * Write/Read 32 bit value to/from DDS_AD9912 user logic memory (BRAM).
 *
 * @param   Address is the memory address of the DDS_AD9912 device.
 * @param   Data is the value written to user logic memory.
 *
 * @return  The data from the user logic memory.
 *
 * @note
 * C-style signature:
 * 	void DDS_AD9912_mWriteMemory(Xuint32 Address, Xuint32 Data)
 * 	Xuint32 DDS_AD9912_mReadMemory(Xuint32 Address)
 *
 */
#define DDS_AD9912_mWriteMemory(Address, Data) \
 	Xil_Out32(Address, (Xuint32)(Data))
#define DDS_AD9912_mReadMemory(Address) \
 	Xil_In32(Address)

/************************** Function Prototypes ****************************/


/**
 *
 * Run a self-test on the driver/device. Note this may be a destructive test if
 * resets of the device are performed.
 *
 * If the hardware system is not built correctly, this function may never
 * return to the caller.
 *
 * @param   baseaddr_p is the base address of the DDS_AD9912 instance to be worked on.
 *
 * @return
 *
 *    - XST_SUCCESS   if all self-test code passed
 *    - XST_FAILURE   if any self-test code failed
 *
 * @note    Caching must be turned off for this function to work.
 * @note    Self test may fail if data memory and device are not on the same bus.
 *
 */
XStatus DDS_AD9912_SelfTest(void * baseaddr_p);

#endif /** DDS_AD9912_H */
