/*****************************************************************************
* Filename:          Y:\Documents\ionetix\firmware\LLRFv1XPS/drivers/llrf_v1_00_a/src/llrf.h
* Version:           1.00.a
* Description:       llrf Driver Header File
* Date:              Thu Mar 27 22:21:26 2014 (by Create and Import Peripheral Wizard)
*****************************************************************************/

#ifndef LLRF_H
#define LLRF_H

/***************************** Include Files *******************************/

#include "xbasic_types.h"
#include "xstatus.h"
#include "xil_io.h"

/************************** Constant Definitions ***************************/


/**************************** Type Definitions *****************************/


/**
 *
 * Write/Read 32 bit value to/from LLRF user logic memory (BRAM).
 *
 * @param   Address is the memory address of the LLRF device.
 * @param   Data is the value written to user logic memory.
 *
 * @return  The data from the user logic memory.
 *
 * @note
 * C-style signature:
 * 	void LLRF_mWriteMemory(Xuint32 Address, Xuint32 Data)
 * 	Xuint32 LLRF_mReadMemory(Xuint32 Address)
 *
 */
#define LLRF_mWriteMemory(Address, Data) \
 	Xil_Out32(Address, (Xuint32)(Data))
#define LLRF_mReadMemory(Address) \
 	Xil_In32(Address)

/************************** Function Prototypes ****************************/


/**
 *
 * Run a self-test on the driver/device. Note this may be a destructive test if
 * resets of the device are performed.
 *
 * If the hardware system is not built correctly, this function may never
 * return to the caller.
 *
 * @param   baseaddr_p is the base address of the LLRF instance to be worked on.
 *
 * @return
 *
 *    - XST_SUCCESS   if all self-test code passed
 *    - XST_FAILURE   if any self-test code failed
 *
 * @note    Caching must be turned off for this function to work.
 * @note    Self test may fail if data memory and device are not on the same bus.
 *
 */
XStatus LLRF_SelfTest(void * baseaddr_p);

#endif /** LLRF_H */
