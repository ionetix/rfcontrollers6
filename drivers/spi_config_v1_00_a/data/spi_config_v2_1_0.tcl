##############################################################################
## Filename:          Y:\Documents\ionetix\firmware\LLRFv1XPS/drivers/spi_config_v1_00_a/data/spi_config_v2_1_0.tcl
## Description:       Microprocess Driver Command (tcl)
## Date:              Sun Apr 06 23:10:25 2014 (by Create and Import Peripheral Wizard)
##############################################################################

#uses "xillib.tcl"

proc generate {drv_handle} {
  xdefine_include_file $drv_handle "xparameters.h" "spi_config" "NUM_INSTANCES" "DEVICE_ID" "C_BASEADDR" "C_HIGHADDR" 
}
